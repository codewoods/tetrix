package Hilos;

import Ventanas.Ventana;
import static java.lang.Thread.sleep;

public class Cronometro extends Thread {

    private volatile boolean running = true;
    private volatile boolean stop = false;
    private int d = 0;
    private int c = 0;
    public Ventana ven;

    public Cronometro(Ventana v) {
        this.ven = v;
    }

    public void run() {
        ven.nuevoElemento();
        stop = true;
        
        try {
            while (stop) {
                do {
                    if (d == 500) {
                        setC(getC() + 1);
                        ven.bajar();
                        d = 0;
                    }
                    sleep(1);
                    d++;
                } while (!running);
            }
        } catch (InterruptedException e) {
            System.out.println("el hilo2 se cayo ");
        }
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public synchronized void pausarHilo() {
        running = false;
    }

    public synchronized void continuarHilo() {
        running = true;
    }

    public synchronized void pararHilo() {
        stop = false;
    }
}

// this.setPriority(Thread.MAX_PRIORITY); 
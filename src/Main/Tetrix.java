package Main;

import Ventanas.Ventana;
import java.awt.Color;

public class Tetrix extends javax.swing.JFrame {

    public Tetrix() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panSuper = new javax.swing.JPanel();
        butJugar = new javax.swing.JButton();
        butRanking = new javax.swing.JButton();
        txtNombre = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tetrix");
        setResizable(false);

        panSuper.setBackground(new java.awt.Color(255, 255, 255));

        butJugar.setFont(new java.awt.Font("Droid Sans", 1, 14)); // NOI18N
        butJugar.setText("Jugar");
        butJugar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        butJugar.setContentAreaFilled(false);
        butJugar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        butJugar.setFocusCycleRoot(true);
        butJugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butJugarActionPerformed(evt);
            }
        });

        butRanking.setFont(new java.awt.Font("Droid Sans", 1, 14)); // NOI18N
        butRanking.setText("Ver ranking");
        butRanking.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        butRanking.setContentAreaFilled(false);
        butRanking.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        butRanking.setEnabled(false);
        butRanking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butRankingActionPerformed(evt);
            }
        });

        txtNombre.setFont(new java.awt.Font("Droid Sans", 1, 14)); // NOI18N
        txtNombre.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtNombre.setMargin(new java.awt.Insets(2, 1, 2, 1));
        txtNombre.setPreferredSize(new java.awt.Dimension(10, 19));
        txtNombre.setSelectionColor(new java.awt.Color(204, 204, 204));
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNombreKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Droid Sans", 1, 14)); // NOI18N
        jLabel1.setText("Tu nombre:");

        javax.swing.GroupLayout panSuperLayout = new javax.swing.GroupLayout(panSuper);
        panSuper.setLayout(panSuperLayout);
        panSuperLayout.setHorizontalGroup(
            panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panSuperLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panSuperLayout.createSequentialGroup()
                        .addComponent(butRanking, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(butJugar, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panSuperLayout.createSequentialGroup()
                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 44, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panSuperLayout.setVerticalGroup(
            panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panSuperLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(butJugar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(butRanking, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE))
                .addGap(15, 15, 15))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panSuper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panSuper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void butRankingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butRankingActionPerformed
        /*VentanaRanking ventanaRanking = null;
        
        if(VentanaRanking.getInstancia()==null){
            ventanaRanking = VentanaRanking.instanciar();
            ventanaRanking.setLocationRelativeTo(null);
            ventanaRanking.setVisible(true);
        } else {
            ventanaRanking = VentanaRanking.getInstancia();
            ventanaRanking.setLocationRelativeTo(null);
            ventanaRanking.setVisible(true);
            this.setExtendedState(ICONIFIED);
            ventanaRanking.requestFocus();
        }*/
        
    }//GEN-LAST:event_butRankingActionPerformed

    private void butJugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butJugarActionPerformed
        if("".equals(txtNombre.getText())){
            txtNombre.setBorder(javax.swing.BorderFactory.createLineBorder(Color.RED));
        } else {
            txtNombre.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
            Ventana v = new Ventana(txtNombre.getText());
            v.setLocationRelativeTo(null);
            v.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_butJugarActionPerformed

    private void txtNombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyPressed
        if (evt.getKeyCode() == evt.VK_ENTER) {
            butJugar.doClick();
        }
    }//GEN-LAST:event_txtNombreKeyPressed

    public static void main(String args[]) {
                Tetrix t;
                t = new Tetrix();
                
                t.setLocationRelativeTo(null);
                t.setVisible(true);
                
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton butJugar;
    private javax.swing.JButton butRanking;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel panSuper;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}

package Ventanas;

import Hilos.Cronometro;
import java.awt.Color;
import java.util.ArrayList;
import java.text.SimpleDateFormat;

public class Ventana extends javax.swing.JFrame {

    /*
     * 1 = o o o o
     * 
     * 2 = o
     *     o o o
     * 
     * 3 =     o
     *     o o o 
     * 
     * 4 = o o
     *     o o
     * 
     * 5 =    o o
     *      o o
     * 
     * 6 =    o
     *      o o o
     * 
     * 7 = o o
     *       o o
     */
    private Cronometro c;
    private ArrayList<javax.swing.JPanel> elementos = new ArrayList<>();
    private int[][] matrizElementos = new int[18][10];
    private int[][] matrizElementosCopia = new int[18][10];
    private int[] aUno = {0, 0, 0, -1, -1, -1, -1, 0, 0, 0};
    private int[][] aDos = {{0, 0, 0, 0, -2, 0, 0, 0, 0, 0}, {0, 0, 0, 0, -2, -2, -2, 0, 0, 0}};
    private int[][] aTres = {{0, 0, 0, 0, 0, 0, -3, 0, 0, 0}, {0, 0, 0, 0, -3, -3, -3, 0, 0, 0}};
    private int[][] aCuatro = {{0, 0, 0, 0, -4, -4, 0, 0, 0, 0}, {0, 0, 0, 0, -4, -4, 0, 0, 0, 0}};
    private int[][] aCinco = {{0, 0, 0, 0, 0, -5, -5, 0, 0, 0}, {0, 0, 0, 0, -5, -5, 0, 0, 0, 0}};
    private int[][] aSeis = {{0, 0, 0, 0, 0, -6, 0, 0, 0, 0}, {0, 0, 0, 0, -6, -6, -6, 0, 0, 0}};
    private int[][] aSiete = {{0, 0, 0, -7, -7, 0, 0, 0, 0, 0}, {0, 0, 0, 0, -7, -7, 0, 0, 0, 0}};
    private int numeroRotacion = 1;
    private boolean juegoPerdido = false;
    private int puntos = 0;
    private final int[] aUnoLateral = {1, 1, 1, 1, 0, 0, 0, 0};
    private final int[] aDosLateral = {2, 0, 0, 0, 2, 2, 2, 0};
    private final int[] aTresLateral = {0, 0, 3, 0, 3, 3, 3, 0};
    private final int[] aCuatroLateral = {0, 4, 4, 0, 0, 4, 4, 0};
    private final int[] aCincoLateral = {0, 5, 5, 0, 5, 5, 0, 0};
    private final int[] aSeisLateral = {0, 6, 0, 0, 6, 6, 6, 0};
    private final int[] aSieteLateral = {7, 7, 0, 0, 0, 7, 7, 0};
    private ArrayList<javax.swing.JPanel> elementosLateral = new ArrayList<>();
    private int[][] matrizElementosLateral = new int[3][8];
    private int[] colaElementos = new int[3];
    private int elementoActual;
    private String nombre;
    private int id;

    public int getNumeroAleatorio() {
        int numeroAleatorio = 0;

        while (numeroAleatorio == 0) {
            numeroAleatorio = (int) (Math.floor(Math.random() * 7 + 1));
        }
        return numeroAleatorio;
    }

    public Ventana(String nom) {
        initComponents();

        this.nombre = nom;

        this.setTitle("Tetrix - " + nombre);

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        java.util.Date now = new java.util.Date();

        elementos.add(e1);
        elementos.add(e2);
        elementos.add(e3);
        elementos.add(e4);
        elementos.add(e5);
        elementos.add(e6);
        elementos.add(e7);
        elementos.add(e8);
        elementos.add(e9);
        elementos.add(e10);
        elementos.add(e11);
        elementos.add(e12);
        elementos.add(e13);
        elementos.add(e14);
        elementos.add(e15);
        elementos.add(e16);
        elementos.add(e17);
        elementos.add(e18);
        elementos.add(e19);
        elementos.add(e20);
        elementos.add(e21);
        elementos.add(e22);
        elementos.add(e23);
        elementos.add(e24);
        elementos.add(e25);
        elementos.add(e26);
        elementos.add(e27);
        elementos.add(e28);
        elementos.add(e29);
        elementos.add(e30);
        elementos.add(e31);
        elementos.add(e32);
        elementos.add(e33);
        elementos.add(e34);
        elementos.add(e35);
        elementos.add(e36);
        elementos.add(e37);
        elementos.add(e38);
        elementos.add(e39);
        elementos.add(e40);
        elementos.add(e41);
        elementos.add(e42);
        elementos.add(e43);
        elementos.add(e44);
        elementos.add(e45);
        elementos.add(e46);
        elementos.add(e47);
        elementos.add(e48);
        elementos.add(e49);
        elementos.add(e50);
        elementos.add(e51);
        elementos.add(e52);
        elementos.add(e53);
        elementos.add(e54);
        elementos.add(e55);
        elementos.add(e56);
        elementos.add(e57);
        elementos.add(e58);
        elementos.add(e59);
        elementos.add(e60);
        elementos.add(e61);
        elementos.add(e62);
        elementos.add(e63);
        elementos.add(e64);
        elementos.add(e65);
        elementos.add(e66);
        elementos.add(e67);
        elementos.add(e68);
        elementos.add(e69);
        elementos.add(e70);
        elementos.add(e71);
        elementos.add(e72);
        elementos.add(e73);
        elementos.add(e74);
        elementos.add(e75);
        elementos.add(e76);
        elementos.add(e77);
        elementos.add(e78);
        elementos.add(e79);
        elementos.add(e80);
        elementos.add(e81);
        elementos.add(e82);
        elementos.add(e83);
        elementos.add(e84);
        elementos.add(e85);
        elementos.add(e86);
        elementos.add(e87);
        elementos.add(e88);
        elementos.add(e89);
        elementos.add(e90);
        elementos.add(e91);
        elementos.add(e92);
        elementos.add(e93);
        elementos.add(e94);
        elementos.add(e95);
        elementos.add(e96);
        elementos.add(e97);
        elementos.add(e98);
        elementos.add(e99);
        elementos.add(e100);
        elementos.add(e101);
        elementos.add(e102);
        elementos.add(e103);
        elementos.add(e104);
        elementos.add(e105);
        elementos.add(e106);
        elementos.add(e107);
        elementos.add(e108);
        elementos.add(e109);
        elementos.add(e110);
        elementos.add(e111);
        elementos.add(e112);
        elementos.add(e113);
        elementos.add(e114);
        elementos.add(e115);
        elementos.add(e116);
        elementos.add(e117);
        elementos.add(e118);
        elementos.add(e119);
        elementos.add(e120);
        elementos.add(e121);
        elementos.add(e122);
        elementos.add(e123);
        elementos.add(e124);
        elementos.add(e125);
        elementos.add(e126);
        elementos.add(e127);
        elementos.add(e128);
        elementos.add(e129);
        elementos.add(e130);
        elementos.add(e131);
        elementos.add(e132);
        elementos.add(e133);
        elementos.add(e134);
        elementos.add(e135);
        elementos.add(e136);
        elementos.add(e137);
        elementos.add(e138);
        elementos.add(e139);
        elementos.add(e140);
        elementos.add(e141);
        elementos.add(e142);
        elementos.add(e143);
        elementos.add(e144);
        elementos.add(e145);
        elementos.add(e146);
        elementos.add(e147);
        elementos.add(e148);
        elementos.add(e149);
        elementos.add(e150);
        elementos.add(e151);
        elementos.add(e152);
        elementos.add(e153);
        elementos.add(e154);
        elementos.add(e155);
        elementos.add(e156);
        elementos.add(e157);
        elementos.add(e158);
        elementos.add(e159);
        elementos.add(e160);
        elementos.add(e161);
        elementos.add(e162);
        elementos.add(e163);
        elementos.add(e164);
        elementos.add(e165);
        elementos.add(e166);
        elementos.add(e167);
        elementos.add(e168);
        elementos.add(e169);
        elementos.add(e170);
        elementos.add(e171);
        elementos.add(e172);
        elementos.add(e173);
        elementos.add(e174);
        elementos.add(e175);
        elementos.add(e176);
        elementos.add(e177);
        elementos.add(e178);
        elementos.add(e179);
        elementos.add(e180);

        elementosLateral.add(lat1);
        elementosLateral.add(lat2);
        elementosLateral.add(let4);
        elementosLateral.add(let3);
        elementosLateral.add(lat5);
        elementosLateral.add(lat6);
        elementosLateral.add(lat7);
        elementosLateral.add(lat8);
        elementosLateral.add(lat9);
        elementosLateral.add(lat10);
        elementosLateral.add(let12);
        elementosLateral.add(let11);
        elementosLateral.add(lat13);
        elementosLateral.add(lat14);
        elementosLateral.add(lat15);
        elementosLateral.add(lat16);
        elementosLateral.add(lat17);
        elementosLateral.add(lat18);
        elementosLateral.add(let20);
        elementosLateral.add(let19);
        elementosLateral.add(lat21);
        elementosLateral.add(lat22);
        elementosLateral.add(lat23);
        elementosLateral.add(lat24);

        for (int i = 0; i < 3; i++) {
            colaElementos[i] = getNumeroAleatorio();
        }

        txtPuntos.setText(String.valueOf(puntos));

        c = new Cronometro(this);
        c.start();
    }

    public synchronized void nuevoElemento() {

        if (juegoPerdido) {
            return;
        }

        if (!analizarJuego()) {
            return;
        }

        elementoActual = colaElementos[0];

        switch (elementoActual) {
            case 1: {
                matrizElementos[0] = aUno.clone();
                break;
            }
            case 2: {
                matrizElementos[0] = aDos[0].clone();
                matrizElementos[1] = aDos[1].clone();
                break;
            }
            case 3: {
                matrizElementos[0] = aTres[0].clone();
                matrizElementos[1] = aTres[1].clone();
                break;
            }
            case 4: {
                matrizElementos[0] = aCuatro[0].clone();
                matrizElementos[1] = aCuatro[1].clone();
                break;
            }
            case 5: {
                matrizElementos[0] = aCinco[0].clone();
                matrizElementos[1] = aCinco[1].clone();
                break;
            }
            case 6: {
                matrizElementos[0] = aSeis[0].clone();
                matrizElementos[1] = aSeis[1].clone();
                break;
            }
            case 7: {
                matrizElementos[0] = aSiete[0].clone();
                matrizElementos[1] = aSiete[1].clone();
                break;
            }
        }

        colaElementos[0] = colaElementos[1];
        colaElementos[1] = colaElementos[2];
        colaElementos[2] = getNumeroAleatorio();

        for (int i = 0; i < 3; i++) {
            switch (colaElementos[i]) {
                case 1: {
                    matrizElementosLateral[i] = aUnoLateral.clone();
                    break;
                }
                case 2: {
                    matrizElementosLateral[i] = aDosLateral.clone();
                    break;
                }
                case 3: {
                    matrizElementosLateral[i] = aTresLateral.clone();
                    break;
                }
                case 4: {
                    matrizElementosLateral[i] = aCuatroLateral.clone();
                    break;
                }
                case 5: {
                    matrizElementosLateral[i] = aCincoLateral.clone();
                    break;
                }
                case 6: {
                    matrizElementosLateral[i] = aSeisLateral.clone();
                    break;
                }
                case 7: {
                    matrizElementosLateral[i] = aSieteLateral.clone();
                    break;
                }
            }
        }

        numeroRotacion = 1;
        colorear();
        colorearLateral();

    }

    public void colorearLateral() {
        for (int i = 0; i < 3; i++) {
            for (int e = 0; e < 8; e++) {
                switch (matrizElementosLateral[i][e]) {
                    case 0: {
                        elementosLateral.get((i * 8) + e).setBackground(new Color(255, 255, 255));
                        elementosLateral.get((i * 8) + e).setBorder(null);
                        elementosLateral.get((i * 8) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                    case 1: {
                        elementosLateral.get((i * 8) + e).setBackground(Color.CYAN);
                        elementosLateral.get((i * 8) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementosLateral.get((i * 8) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                    case 2: {
                        elementosLateral.get((i * 8) + e).setBackground(Color.BLUE);
                        elementosLateral.get((i * 8) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementosLateral.get((i * 8) + e).setPreferredSize(new java.awt.Dimension(33, 33));

                        break;
                    }
                    case 3: {
                        elementosLateral.get((i * 8) + e).setBackground(new Color(255, 127, 0));
                        elementosLateral.get((i * 8) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementosLateral.get((i * 8) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                    case 4: {
                        elementosLateral.get((i * 8) + e).setBackground(Color.YELLOW);
                        elementosLateral.get((i * 8) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementosLateral.get((i * 8) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                    case 5: {
                        elementosLateral.get((i * 8) + e).setBackground(Color.GREEN);
                        elementosLateral.get((i * 8) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementosLateral.get((i * 8) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                    case 6: {
                        elementosLateral.get((i * 8) + e).setBackground(new Color(160, 0, 255));
                        elementosLateral.get((i * 8) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementosLateral.get((i * 8) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                    case 7: {
                        elementosLateral.get((i * 8) + e).setBackground(Color.RED);
                        elementosLateral.get((i * 8) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementosLateral.get((i * 8) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                }
            }
        }
    }

    public void colorear() {
        for (int i = 0; i < 18; i++) {
            for (int e = 0; e < 10; e++) {
                //System.out.print(matrizElementos[i][e] + "\t");
                switch (Math.abs(matrizElementos[i][e])) {
                    case 0: {
                        elementos.get((i * 10) + e).setBackground(new Color(25, 25, 25));
                        elementos.get((i * 10) + e).setBorder(null);
                        elementos.get((i * 10) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                    case 1: {
                        elementos.get((i * 10) + e).setBackground(Color.CYAN);
                        elementos.get((i * 10) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementos.get((i * 10) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                    case 2: {
                        elementos.get((i * 10) + e).setBackground(Color.BLUE);
                        elementos.get((i * 10) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementos.get((i * 10) + e).setPreferredSize(new java.awt.Dimension(33, 33));

                        break;
                    }
                    case 3: {
                        elementos.get((i * 10) + e).setBackground(new Color(255, 127, 0));
                        elementos.get((i * 10) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementos.get((i * 10) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                    case 4: {
                        elementos.get((i * 10) + e).setBackground(Color.YELLOW);
                        elementos.get((i * 10) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementos.get((i * 10) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                    case 5: {
                        elementos.get((i * 10) + e).setBackground(Color.GREEN);
                        elementos.get((i * 10) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementos.get((i * 10) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                    case 6: {
                        elementos.get((i * 10) + e).setBackground(new Color(160, 0, 255));
                        elementos.get((i * 10) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementos.get((i * 10) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                    case 7: {
                        elementos.get((i * 10) + e).setBackground(Color.RED);
                        elementos.get((i * 10) + e).setBorder(javax.swing.BorderFactory.createBevelBorder(0));
                        elementos.get((i * 10) + e).setPreferredSize(new java.awt.Dimension(33, 33));
                        break;
                    }
                }
            }
            System.out.print("\n");

        }
    }

    public synchronized void bajar() {
        verificarYEstancarElementos();

        for (int i = 17; i >= 0; i--) {
            for (int e = 9; e >= 0; e--) {
                if (i > 0 && matrizElementos[i - 1][e] < 0) {

                    if (matrizElementos[i][e] > 0) {
                        estancarElementos();
                    } else {
                        matrizElementos[i][e] = matrizElementos[i - 1][e];
                        matrizElementos[i - 1][e] = 0;
                    }
                }
            }
        }
        
        colorear();

        if (!hayNegatividades()) {
            nuevoElemento();
        }
    }

    public void estancarElementos() {

        for (int i = 0; i < 18; i++) {
            for (int e = 0; e < 10; e++) {
                if (matrizElementos[i][e] < 0) {
                    matrizElementos[i][e] = matrizElementos[i][e] * -1;
                }
            }
        }
    }

    public void verificarYEstancarElementos() {
        for (int i = 0; i < 18; i++) {
            for (int e = 0; e < 10; e++) {
                if (i == 17 && matrizElementos[i][e] < 0) {
                    estancarElementos();
                    return;
                }
                if (matrizElementos[i][e] < 0 && matrizElementos[i + 1][e] > 0 && i < 17) {
                    estancarElementos();
                    return;
                }
            }
        }
    }

    public boolean hayNegatividades() {
        for (int i = 0; i < 18; i++) {
            for (int e = 0; e < 10; e++) {
                if (matrizElementos[i][e] < 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public void moverDerecha() {
        c.pausarHilo();

        matrizElementosCopia = matrizElementos;

        for (int i = 9; i >= 0; i--) {
            for (int e = 17; e >= 0; e--) {
                if (matrizElementos[e][9] < 0) {
                    c.continuarHilo();
                    return;
                }
                if (i > 0) {
                    if (matrizElementos[e][i] > 0 && matrizElementos[e][i - 1] < 0) {
                        c.continuarHilo();
                        return;
                    }
                }
            }
        }

        for (int i = 9; i >= 0; i--) {
            for (int e = 17; e >= 0; e--) {
                if (i > 0) {
                    if (matrizElementosCopia[e][i - 1] < 0) {
                        matrizElementosCopia[e][i] = matrizElementosCopia[e][i - 1];
                        matrizElementosCopia[e][i - 1] = 0;
                    }
                }
            }
        }
        matrizElementos = matrizElementosCopia;
        c.continuarHilo();
    }

    public void moverIzquierda() {
        c.pausarHilo();
        matrizElementosCopia = matrizElementos;

        for (int i = 0; i < 10; i++) {
            for (int e = 0; e < 18; e++) {
                if (matrizElementos[e][0] < 0) {
                    c.continuarHilo();
                    return;
                }
                if (i < 9) {
                    if (matrizElementos[e][i] > 0 && matrizElementos[e][i + 1] < 0) {
                        c.continuarHilo();
                        return;
                    }
                }
            }
        }

        for (int i = 0; i < 10; i++) {
            for (int e = 0; e < 18; e++) {
                if (i < 9) {

                    if (matrizElementosCopia[e][i + 1] < 0) {
                        matrizElementosCopia[e][i] = matrizElementosCopia[e][i + 1];
                        matrizElementosCopia[e][i + 1] = 0;
                    }
                }
            }
        }
        matrizElementos = matrizElementosCopia;
        c.continuarHilo();
    }

    public void rotarPieza1() {
        switch (numeroRotacion) {
            case 1: {
                for (int i = 0; i < 10; i++) {
                    for (int e = 0; e < 18; e++) {
                        if (matrizElementos[e][i] == -1) {
                            if (e > 1 && e < 16) {
                                if (matrizElementos[e - 1][i + 1] == 0 && matrizElementos[e - 2][i + 1] == 0 && matrizElementos[e + 1][i + 1] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e][i + 2] = 0;
                                    matrizElementos[e][i + 3] = 0;
                                    matrizElementos[e - 1][i + 1] = -1;
                                    matrizElementos[e - 2][i + 1] = -1;
                                    matrizElementos[e + 1][i + 1] = -1;
                                    numeroRotacion = 2;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
            case 2: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -1) {
                            if (i > 0 && i < 8) {
                                if (matrizElementos[e + 1][i - 1] == 0 && matrizElementos[e + 1][i + 1] == 0 && matrizElementos[e + 1][i + 2] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e + 2][i] = 0;
                                    matrizElementos[e + 3][i] = 0;
                                    matrizElementos[e + 1][i - 1] = -1;
                                    matrizElementos[e + 1][i + 1] = -1;
                                    matrizElementos[e + 1][i + 2] = -1;
                                    numeroRotacion = 3;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }

            case 3: {
                for (int i = 0; i < 10; i++) {
                    for (int e = 0; e < 18; e++) {
                        if (matrizElementos[e][i] == -1) {
                            if (e > 1 && e < 16) {
                                if (matrizElementos[e - 1][i + 2] == 0 && matrizElementos[e - 2][i + 2] == 0 && matrizElementos[e + 1][i + 2] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e][i + 1] = 0;
                                    matrizElementos[e][i + 3] = 0;
                                    matrizElementos[e - 1][i + 2] = -1;
                                    matrizElementos[e - 2][i + 2] = -1;
                                    matrizElementos[e + 1][i + 2] = -1;
                                    numeroRotacion = 4;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
            case 4: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -1) {
                            if (i > 1 && i < 9) {
                                if (matrizElementos[e + 2][i + 1] == 0 && matrizElementos[e + 2][i - 1] == 0 && matrizElementos[e + 2][i - 2] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e + 1][i] = 0;
                                    matrizElementos[e + 3][i] = 0;
                                    matrizElementos[e + 2][i + 1] = -1;
                                    matrizElementos[e + 2][i - 1] = -1;
                                    matrizElementos[e + 2][i - 2] = -1;
                                    numeroRotacion = 1;
                                    bajar();
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
        }
    }

    public void rotarPieza2() {
        switch (numeroRotacion) {
            case 1: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -2) {
                            if (e < 16) {
                                if (matrizElementos[e][i + 1] == 0 && matrizElementos[e][i + 2] == 0 && matrizElementos[e + 2][i + 1] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e + 1][i] = 0;
                                    matrizElementos[e + 1][i + 2] = 0;
                                    matrizElementos[e][i + 1] = -2;
                                    matrizElementos[e][i + 2] = -2;
                                    matrizElementos[e + 2][i + 1] = -2;
                                    numeroRotacion = 2;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
            case 2: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -2) {
                            if (i > 0) {
                                if (matrizElementos[e + 1][i + 1] == 0 && matrizElementos[e + 2][i + 1] == 0 && matrizElementos[e + 1][i - 1] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e][i + 1] = 0;
                                    matrizElementos[e + 2][i] = 0;
                                    matrizElementos[e + 1][i + 1] = -2;
                                    matrizElementos[e + 2][i + 1] = -2;
                                    matrizElementos[e + 1][i - 1] = -2;
                                    numeroRotacion = 3;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
            case 3: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -2) {
                            if (e > 0) {
                                if (matrizElementos[e - 1][i + 1] == 0 && matrizElementos[e + 1][i] == 0 && matrizElementos[e + 1][i + 1] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e][i + 2] = 0;
                                    matrizElementos[e + 1][i + 2] = 0;
                                    matrizElementos[e - 1][i + 1] = -2;
                                    matrizElementos[e + 1][i] = -2;
                                    matrizElementos[e + 1][i + 1] = -2;
                                    numeroRotacion = 4;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
            case 4: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -2) {
                            if (i < 9) {
                                if (matrizElementos[e][i - 1] == 0 && matrizElementos[e + 1][i - 1] == 0 && matrizElementos[e + 1][i + 1] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e + 2][i - 1] = 0;
                                    matrizElementos[e + 2][i] = 0;
                                    matrizElementos[e][i - 1] = -2;
                                    matrizElementos[e + 1][i - 1] = -2;
                                    matrizElementos[e + 1][i + 1] = -2;
                                    numeroRotacion = 1;
                                    bajar();
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
        }
    }

    public void rotarPieza3() {
        switch (numeroRotacion) {
            case 1: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -3) {
                            if (e < 16) {
                                if (matrizElementos[e][i - 1] == 0 && matrizElementos[e + 2][i - 1] == 0 && matrizElementos[e + 2][i] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e + 1][i] = 0;
                                    matrizElementos[e + 1][i - 2] = 0;
                                    matrizElementos[e][i - 1] = -3;
                                    matrizElementos[e + 2][i - 1] = -3;
                                    matrizElementos[e + 2][i] = -3;
                                    numeroRotacion = 2;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
            case 2: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -3) {
                            if (i > 0) {
                                if (matrizElementos[e + 1][i + 1] == 0 && matrizElementos[e + 1][i - 1] == 0 && matrizElementos[e + 2][i - 1] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e + 2][i] = 0;
                                    matrizElementos[e + 2][i + 1] = 0;
                                    matrizElementos[e + 1][i + 1] = -3;
                                    matrizElementos[e + 1][i - 1] = -3;
                                    matrizElementos[e + 2][i - 1] = -3;
                                    numeroRotacion = 3;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
            case 3: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -3) {
                            if (e > 0) {
                                if (matrizElementos[e - 1][i] == 0 && matrizElementos[e - 1][i + 1] == 0 && matrizElementos[e + 1][i + 1] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e][i + 2] = 0;
                                    matrizElementos[e + 1][i] = 0;
                                    matrizElementos[e - 1][i] = -3;
                                    matrizElementos[e - 1][i + 1] = -3;
                                    matrizElementos[e + 1][i + 1] = -3;
                                    numeroRotacion = 4;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
            case 4: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -3) {
                            if (e > 0) {
                                if (matrizElementos[e + 1][i] == 0 && matrizElementos[e][i + 2] == 0 && matrizElementos[e + 1][i + 2] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e][i + 1] = 0;
                                    matrizElementos[e + 2][i + 1] = 0;
                                    matrizElementos[e + 1][i] = -3;
                                    matrizElementos[e][i + 2] = -3;
                                    matrizElementos[e + 1][i + 2] = -3;
                                    numeroRotacion = 1;
                                    bajar();
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
        }
    }

    public void rotarPieza5() {
        switch (numeroRotacion) {
            case 1: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -5) {
                            if (e > 0) {
                                if (matrizElementos[e - 1][i] == 0 && matrizElementos[e + 1][i + 1] == 0) {
                                    matrizElementos[e + 1][i - 1] = 0;
                                    matrizElementos[e + 1][i] = 0;
                                    matrizElementos[e - 1][i] = -5;
                                    matrizElementos[e + 1][i + 1] = -5;
                                    numeroRotacion = 2;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
            case 2: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -5) {
                            if (i > 0) {
                                if (matrizElementos[e + 2][i] == 0 && matrizElementos[e + 2][i - 1] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e + 2][i + 1] = 0;
                                    matrizElementos[e + 2][i] = -5;
                                    matrizElementos[e + 2][i - 1] = -5;
                                    numeroRotacion = 1;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
        }
    }

    public void rotarPieza6() {
        switch (numeroRotacion) {
            case 1: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -6) {
                            if (e < 18) {
                                if (matrizElementos[e + 2][i] == 0) {
                                    matrizElementos[e + 1][i - 1] = 0;
                                    matrizElementos[e + 2][i] = -6;
                                    numeroRotacion = 2;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
            case 2: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -6) {
                            if (i > 0) {
                                if (matrizElementos[e + 1][i - 1] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e + 1][i - 1] = -6;
                                    numeroRotacion = 3;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
            case 3: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -6) {
                            if (matrizElementos[e - 1][i + 1] == 0) {
                                matrizElementos[e][i + 2] = 0;
                                matrizElementos[e - 1][i + 1] = -6;
                                numeroRotacion = 4;
                                return;
                            }
                            return;
                        }
                    }
                }
            }
            case 4: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {

                        if (matrizElementos[e][i] == -6) {
                            if (i != 9) {
                                if (matrizElementos[e + 1][i + 1] == 0) {
                                    matrizElementos[e + 2][i] = 0;
                                    matrizElementos[e + 1][i + 1] = -6;
                                    numeroRotacion = 1;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
        }
    }

    public void rotarPieza7() {
        switch (numeroRotacion) {
            case 1: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -7) {
                            if (e > 0) {
                                if (matrizElementos[e + 1][i] == 0 && matrizElementos[e - 1][i + 1] == 0) {
                                    matrizElementos[e + 1][i + 1] = 0;
                                    matrizElementos[e + 1][i + 2] = 0;
                                    matrizElementos[e + 1][i] = -7;
                                    matrizElementos[e - 1][i + 1] = -7;
                                    numeroRotacion = 2;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
            case 2: {
                for (int e = 0; e < 18; e++) {
                    for (int i = 0; i < 10; i++) {
                        if (matrizElementos[e][i] == -7) {
                            if (i < 9) {
                                if (matrizElementos[e + 2][i] == 0 && matrizElementos[e + 2][i + 1] == 0) {
                                    matrizElementos[e][i] = 0;
                                    matrizElementos[e + 2][i - 1] = 0;
                                    matrizElementos[e + 2][i] = -7;
                                    matrizElementos[e + 2][i + 1] = -7;
                                    numeroRotacion = 1;
                                    return;
                                }
                            }
                            return;
                        }
                    }
                }
            }
        }
    }

    public void rotarPieza() {
        switch (elementoActual) {
            case 1: {
                rotarPieza1();
                break;
            }
            case 2: {
                rotarPieza2();
                break;
            }
            case 3: {
                rotarPieza3();
                break;
            }
            case 4: {
                break;
            }
            case 5: {
                rotarPieza5();
                break;
            }
            case 6: {
                rotarPieza6();
                break;
            }
            case 7: {
                rotarPieza7();
                break;
            }
        }
    }

    public synchronized boolean analizarJuego() {
        int puntosJugada = 0;

        if (matrizElementos[0][0] > 0 || matrizElementos[0][1] > 0 || matrizElementos[0][2] > 0 || matrizElementos[0][3] > 0 || matrizElementos[0][4] > 0 || matrizElementos[0][5] > 0 || matrizElementos[0][6] > 0 || matrizElementos[0][7] > 0 || matrizElementos[0][8] > 0 || matrizElementos[0][9] > 0) {
            juegoPerdido = true;
            c.pausarHilo();
            c.pararHilo();
            GameOver gm = new GameOver(this);
            gm.setLocationRelativeTo(null);
            gm.setVisible(true);
            return false;
        }

        puntosJugada = 0;

        for (int i = 17; i > 0; i--) {
            if (matrizElementos[i][0] > 0 && matrizElementos[i][1] > 0 && matrizElementos[i][2] > 0 && matrizElementos[i][3] > 0 && matrizElementos[i][4] > 0 && matrizElementos[i][5] > 0 && matrizElementos[i][6] > 0 && matrizElementos[i][7] > 0 && matrizElementos[i][8] > 0 && matrizElementos[i][9] > 0) {
                puntosJugada++;
                for (int e = i; e > 0; e--) {
                    matrizElementos[e] = matrizElementos[e - 1].clone();
                }
                i = i + 1;
            }
        }

        switch (puntosJugada) {
            case 1: {
                puntos = puntos + 30;
                break;
            }
            case 2: {
                puntos = puntos + 30 + 7;
                break;
            }
            case 3: {
                puntos = puntos + 30 + 7 + 14;
                break;
            }
            case 4: {
                puntos = puntos + 30 + 7 + 14 + 21;
                break;
            }
            default:
                break;
        }

        txtPuntos.setText(String.valueOf(puntos));
        return true;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panSuper = new javax.swing.JPanel();
        panLateral = new javax.swing.JPanel();
        txtPuntos = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        lat1 = new javax.swing.JPanel();
        lat2 = new javax.swing.JPanel();
        let4 = new javax.swing.JPanel();
        let3 = new javax.swing.JPanel();
        lat5 = new javax.swing.JPanel();
        lat6 = new javax.swing.JPanel();
        lat7 = new javax.swing.JPanel();
        lat8 = new javax.swing.JPanel();
        lat9 = new javax.swing.JPanel();
        lat10 = new javax.swing.JPanel();
        let12 = new javax.swing.JPanel();
        let11 = new javax.swing.JPanel();
        lat13 = new javax.swing.JPanel();
        lat14 = new javax.swing.JPanel();
        lat15 = new javax.swing.JPanel();
        lat16 = new javax.swing.JPanel();
        lat17 = new javax.swing.JPanel();
        lat18 = new javax.swing.JPanel();
        let20 = new javax.swing.JPanel();
        let19 = new javax.swing.JPanel();
        lat21 = new javax.swing.JPanel();
        lat22 = new javax.swing.JPanel();
        lat23 = new javax.swing.JPanel();
        lat24 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        e1 = new javax.swing.JPanel();
        e2 = new javax.swing.JPanel();
        e3 = new javax.swing.JPanel();
        e4 = new javax.swing.JPanel();
        e5 = new javax.swing.JPanel();
        e6 = new javax.swing.JPanel();
        e7 = new javax.swing.JPanel();
        e8 = new javax.swing.JPanel();
        e9 = new javax.swing.JPanel();
        e10 = new javax.swing.JPanel();
        e11 = new javax.swing.JPanel();
        e12 = new javax.swing.JPanel();
        e13 = new javax.swing.JPanel();
        e14 = new javax.swing.JPanel();
        e15 = new javax.swing.JPanel();
        e16 = new javax.swing.JPanel();
        e17 = new javax.swing.JPanel();
        e18 = new javax.swing.JPanel();
        e19 = new javax.swing.JPanel();
        e20 = new javax.swing.JPanel();
        e21 = new javax.swing.JPanel();
        e22 = new javax.swing.JPanel();
        e23 = new javax.swing.JPanel();
        e24 = new javax.swing.JPanel();
        e25 = new javax.swing.JPanel();
        e26 = new javax.swing.JPanel();
        e27 = new javax.swing.JPanel();
        e28 = new javax.swing.JPanel();
        e29 = new javax.swing.JPanel();
        e30 = new javax.swing.JPanel();
        e31 = new javax.swing.JPanel();
        e32 = new javax.swing.JPanel();
        e33 = new javax.swing.JPanel();
        e34 = new javax.swing.JPanel();
        e35 = new javax.swing.JPanel();
        e36 = new javax.swing.JPanel();
        e37 = new javax.swing.JPanel();
        e38 = new javax.swing.JPanel();
        e39 = new javax.swing.JPanel();
        e40 = new javax.swing.JPanel();
        e41 = new javax.swing.JPanel();
        e42 = new javax.swing.JPanel();
        e43 = new javax.swing.JPanel();
        e44 = new javax.swing.JPanel();
        e45 = new javax.swing.JPanel();
        e46 = new javax.swing.JPanel();
        e47 = new javax.swing.JPanel();
        e48 = new javax.swing.JPanel();
        e49 = new javax.swing.JPanel();
        e50 = new javax.swing.JPanel();
        e51 = new javax.swing.JPanel();
        e52 = new javax.swing.JPanel();
        e53 = new javax.swing.JPanel();
        e54 = new javax.swing.JPanel();
        e55 = new javax.swing.JPanel();
        e56 = new javax.swing.JPanel();
        e57 = new javax.swing.JPanel();
        e58 = new javax.swing.JPanel();
        e59 = new javax.swing.JPanel();
        e60 = new javax.swing.JPanel();
        e61 = new javax.swing.JPanel();
        e62 = new javax.swing.JPanel();
        e63 = new javax.swing.JPanel();
        e64 = new javax.swing.JPanel();
        e65 = new javax.swing.JPanel();
        e66 = new javax.swing.JPanel();
        e67 = new javax.swing.JPanel();
        e68 = new javax.swing.JPanel();
        e69 = new javax.swing.JPanel();
        e70 = new javax.swing.JPanel();
        e71 = new javax.swing.JPanel();
        e72 = new javax.swing.JPanel();
        e73 = new javax.swing.JPanel();
        e74 = new javax.swing.JPanel();
        e75 = new javax.swing.JPanel();
        e76 = new javax.swing.JPanel();
        e77 = new javax.swing.JPanel();
        e78 = new javax.swing.JPanel();
        e79 = new javax.swing.JPanel();
        e80 = new javax.swing.JPanel();
        e81 = new javax.swing.JPanel();
        e82 = new javax.swing.JPanel();
        e83 = new javax.swing.JPanel();
        e84 = new javax.swing.JPanel();
        e85 = new javax.swing.JPanel();
        e86 = new javax.swing.JPanel();
        e87 = new javax.swing.JPanel();
        e88 = new javax.swing.JPanel();
        e89 = new javax.swing.JPanel();
        e90 = new javax.swing.JPanel();
        e91 = new javax.swing.JPanel();
        e92 = new javax.swing.JPanel();
        e93 = new javax.swing.JPanel();
        e94 = new javax.swing.JPanel();
        e95 = new javax.swing.JPanel();
        e96 = new javax.swing.JPanel();
        e97 = new javax.swing.JPanel();
        e98 = new javax.swing.JPanel();
        e99 = new javax.swing.JPanel();
        e100 = new javax.swing.JPanel();
        e101 = new javax.swing.JPanel();
        e102 = new javax.swing.JPanel();
        e103 = new javax.swing.JPanel();
        e104 = new javax.swing.JPanel();
        e105 = new javax.swing.JPanel();
        e106 = new javax.swing.JPanel();
        e107 = new javax.swing.JPanel();
        e108 = new javax.swing.JPanel();
        e109 = new javax.swing.JPanel();
        e110 = new javax.swing.JPanel();
        e111 = new javax.swing.JPanel();
        e112 = new javax.swing.JPanel();
        e113 = new javax.swing.JPanel();
        e114 = new javax.swing.JPanel();
        e115 = new javax.swing.JPanel();
        e116 = new javax.swing.JPanel();
        e117 = new javax.swing.JPanel();
        e118 = new javax.swing.JPanel();
        e119 = new javax.swing.JPanel();
        e120 = new javax.swing.JPanel();
        e121 = new javax.swing.JPanel();
        e122 = new javax.swing.JPanel();
        e123 = new javax.swing.JPanel();
        e124 = new javax.swing.JPanel();
        e125 = new javax.swing.JPanel();
        e126 = new javax.swing.JPanel();
        e127 = new javax.swing.JPanel();
        e128 = new javax.swing.JPanel();
        e129 = new javax.swing.JPanel();
        e130 = new javax.swing.JPanel();
        e131 = new javax.swing.JPanel();
        e132 = new javax.swing.JPanel();
        e133 = new javax.swing.JPanel();
        e134 = new javax.swing.JPanel();
        e135 = new javax.swing.JPanel();
        e136 = new javax.swing.JPanel();
        e137 = new javax.swing.JPanel();
        e138 = new javax.swing.JPanel();
        e139 = new javax.swing.JPanel();
        e140 = new javax.swing.JPanel();
        e141 = new javax.swing.JPanel();
        e142 = new javax.swing.JPanel();
        e143 = new javax.swing.JPanel();
        e144 = new javax.swing.JPanel();
        e145 = new javax.swing.JPanel();
        e146 = new javax.swing.JPanel();
        e147 = new javax.swing.JPanel();
        e148 = new javax.swing.JPanel();
        e149 = new javax.swing.JPanel();
        e150 = new javax.swing.JPanel();
        e151 = new javax.swing.JPanel();
        e152 = new javax.swing.JPanel();
        e153 = new javax.swing.JPanel();
        e154 = new javax.swing.JPanel();
        e155 = new javax.swing.JPanel();
        e156 = new javax.swing.JPanel();
        e157 = new javax.swing.JPanel();
        e158 = new javax.swing.JPanel();
        e159 = new javax.swing.JPanel();
        e160 = new javax.swing.JPanel();
        e161 = new javax.swing.JPanel();
        e162 = new javax.swing.JPanel();
        e163 = new javax.swing.JPanel();
        e164 = new javax.swing.JPanel();
        e165 = new javax.swing.JPanel();
        e166 = new javax.swing.JPanel();
        e167 = new javax.swing.JPanel();
        e168 = new javax.swing.JPanel();
        e169 = new javax.swing.JPanel();
        e170 = new javax.swing.JPanel();
        e171 = new javax.swing.JPanel();
        e172 = new javax.swing.JPanel();
        e173 = new javax.swing.JPanel();
        e174 = new javax.swing.JPanel();
        e175 = new javax.swing.JPanel();
        e176 = new javax.swing.JPanel();
        e177 = new javax.swing.JPanel();
        e178 = new javax.swing.JPanel();
        e179 = new javax.swing.JPanel();
        e180 = new javax.swing.JPanel();
        e182 = new javax.swing.JPanel();
        e183 = new javax.swing.JPanel();
        e187 = new javax.swing.JPanel();
        e188 = new javax.swing.JPanel();
        e189 = new javax.swing.JPanel();
        e190 = new javax.swing.JPanel();
        e191 = new javax.swing.JPanel();
        e192 = new javax.swing.JPanel();
        e193 = new javax.swing.JPanel();
        e194 = new javax.swing.JPanel();
        e195 = new javax.swing.JPanel();
        e196 = new javax.swing.JPanel();
        e197 = new javax.swing.JPanel();
        e198 = new javax.swing.JPanel();
        e199 = new javax.swing.JPanel();
        e200 = new javax.swing.JPanel();
        e201 = new javax.swing.JPanel();
        e202 = new javax.swing.JPanel();
        e203 = new javax.swing.JPanel();
        e204 = new javax.swing.JPanel();
        e225 = new javax.swing.JPanel();
        e226 = new javax.swing.JPanel();
        e227 = new javax.swing.JPanel();
        e228 = new javax.swing.JPanel();
        e229 = new javax.swing.JPanel();
        e230 = new javax.swing.JPanel();
        e231 = new javax.swing.JPanel();
        e232 = new javax.swing.JPanel();
        e233 = new javax.swing.JPanel();
        e234 = new javax.swing.JPanel();
        e235 = new javax.swing.JPanel();
        e236 = new javax.swing.JPanel();
        e237 = new javax.swing.JPanel();
        e238 = new javax.swing.JPanel();
        e239 = new javax.swing.JPanel();
        e240 = new javax.swing.JPanel();
        e241 = new javax.swing.JPanel();
        e242 = new javax.swing.JPanel();
        e243 = new javax.swing.JPanel();
        e244 = new javax.swing.JPanel();
        e245 = new javax.swing.JPanel();
        e246 = new javax.swing.JPanel();
        e247 = new javax.swing.JPanel();
        e248 = new javax.swing.JPanel();
        e249 = new javax.swing.JPanel();
        e250 = new javax.swing.JPanel();
        e251 = new javax.swing.JPanel();
        e252 = new javax.swing.JPanel();
        e253 = new javax.swing.JPanel();
        e254 = new javax.swing.JPanel();
        e255 = new javax.swing.JPanel();
        e256 = new javax.swing.JPanel();
        e257 = new javax.swing.JPanel();
        e258 = new javax.swing.JPanel();
        e259 = new javax.swing.JPanel();
        e260 = new javax.swing.JPanel();
        e261 = new javax.swing.JPanel();
        e262 = new javax.swing.JPanel();
        e263 = new javax.swing.JPanel();
        e264 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        panSuper.setBackground(new java.awt.Color(255, 255, 255));

        panLateral.setBackground(new java.awt.Color(255, 255, 255));
        panLateral.setFocusable(false);

        txtPuntos.setEditable(false);
        txtPuntos.setBackground(new java.awt.Color(0, 0, 0));
        txtPuntos.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        txtPuntos.setForeground(new java.awt.Color(0, 255, 51));
        txtPuntos.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPuntos.setFocusable(false);

        jLabel2.setFont(new java.awt.Font("Droid Sans", 1, 14)); // NOI18N
        jLabel2.setText("Puntos:");

        lat1.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat1Layout = new javax.swing.GroupLayout(lat1);
        lat1.setLayout(lat1Layout);
        lat1Layout.setHorizontalGroup(
            lat1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat1Layout.setVerticalGroup(
            lat1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat2.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat2Layout = new javax.swing.GroupLayout(lat2);
        lat2.setLayout(lat2Layout);
        lat2Layout.setHorizontalGroup(
            lat2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat2Layout.setVerticalGroup(
            lat2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        let4.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout let4Layout = new javax.swing.GroupLayout(let4);
        let4.setLayout(let4Layout);
        let4Layout.setHorizontalGroup(
            let4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        let4Layout.setVerticalGroup(
            let4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        let3.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout let3Layout = new javax.swing.GroupLayout(let3);
        let3.setLayout(let3Layout);
        let3Layout.setHorizontalGroup(
            let3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        let3Layout.setVerticalGroup(
            let3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat5.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat5Layout = new javax.swing.GroupLayout(lat5);
        lat5.setLayout(lat5Layout);
        lat5Layout.setHorizontalGroup(
            lat5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat5Layout.setVerticalGroup(
            lat5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat6.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat6Layout = new javax.swing.GroupLayout(lat6);
        lat6.setLayout(lat6Layout);
        lat6Layout.setHorizontalGroup(
            lat6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat6Layout.setVerticalGroup(
            lat6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat7.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat7Layout = new javax.swing.GroupLayout(lat7);
        lat7.setLayout(lat7Layout);
        lat7Layout.setHorizontalGroup(
            lat7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat7Layout.setVerticalGroup(
            lat7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat8.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat8Layout = new javax.swing.GroupLayout(lat8);
        lat8.setLayout(lat8Layout);
        lat8Layout.setHorizontalGroup(
            lat8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat8Layout.setVerticalGroup(
            lat8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat9.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat9Layout = new javax.swing.GroupLayout(lat9);
        lat9.setLayout(lat9Layout);
        lat9Layout.setHorizontalGroup(
            lat9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat9Layout.setVerticalGroup(
            lat9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat10.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat10Layout = new javax.swing.GroupLayout(lat10);
        lat10.setLayout(lat10Layout);
        lat10Layout.setHorizontalGroup(
            lat10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat10Layout.setVerticalGroup(
            lat10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        let12.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout let12Layout = new javax.swing.GroupLayout(let12);
        let12.setLayout(let12Layout);
        let12Layout.setHorizontalGroup(
            let12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        let12Layout.setVerticalGroup(
            let12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        let11.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout let11Layout = new javax.swing.GroupLayout(let11);
        let11.setLayout(let11Layout);
        let11Layout.setHorizontalGroup(
            let11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        let11Layout.setVerticalGroup(
            let11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat13.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat13Layout = new javax.swing.GroupLayout(lat13);
        lat13.setLayout(lat13Layout);
        lat13Layout.setHorizontalGroup(
            lat13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat13Layout.setVerticalGroup(
            lat13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat14.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat14Layout = new javax.swing.GroupLayout(lat14);
        lat14.setLayout(lat14Layout);
        lat14Layout.setHorizontalGroup(
            lat14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat14Layout.setVerticalGroup(
            lat14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat15.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat15Layout = new javax.swing.GroupLayout(lat15);
        lat15.setLayout(lat15Layout);
        lat15Layout.setHorizontalGroup(
            lat15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat15Layout.setVerticalGroup(
            lat15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat16.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat16Layout = new javax.swing.GroupLayout(lat16);
        lat16.setLayout(lat16Layout);
        lat16Layout.setHorizontalGroup(
            lat16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat16Layout.setVerticalGroup(
            lat16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat17.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat17Layout = new javax.swing.GroupLayout(lat17);
        lat17.setLayout(lat17Layout);
        lat17Layout.setHorizontalGroup(
            lat17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat17Layout.setVerticalGroup(
            lat17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat18.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat18Layout = new javax.swing.GroupLayout(lat18);
        lat18.setLayout(lat18Layout);
        lat18Layout.setHorizontalGroup(
            lat18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat18Layout.setVerticalGroup(
            lat18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        let20.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout let20Layout = new javax.swing.GroupLayout(let20);
        let20.setLayout(let20Layout);
        let20Layout.setHorizontalGroup(
            let20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        let20Layout.setVerticalGroup(
            let20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        let19.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout let19Layout = new javax.swing.GroupLayout(let19);
        let19.setLayout(let19Layout);
        let19Layout.setHorizontalGroup(
            let19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        let19Layout.setVerticalGroup(
            let19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat21.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat21Layout = new javax.swing.GroupLayout(lat21);
        lat21.setLayout(lat21Layout);
        lat21Layout.setHorizontalGroup(
            lat21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat21Layout.setVerticalGroup(
            lat21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat22.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat22Layout = new javax.swing.GroupLayout(lat22);
        lat22.setLayout(lat22Layout);
        lat22Layout.setHorizontalGroup(
            lat22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat22Layout.setVerticalGroup(
            lat22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat23.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat23Layout = new javax.swing.GroupLayout(lat23);
        lat23.setLayout(lat23Layout);
        lat23Layout.setHorizontalGroup(
            lat23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat23Layout.setVerticalGroup(
            lat23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        lat24.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout lat24Layout = new javax.swing.GroupLayout(lat24);
        lat24.setLayout(lat24Layout);
        lat24Layout.setHorizontalGroup(
            lat24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        lat24Layout.setVerticalGroup(
            lat24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        jLabel3.setFont(new java.awt.Font("Droid Sans", 1, 14)); // NOI18N
        jLabel3.setText("Siguiente:");

        javax.swing.GroupLayout panLateralLayout = new javax.swing.GroupLayout(panLateral);
        panLateral.setLayout(panLateralLayout);
        panLateralLayout.setHorizontalGroup(
            panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(txtPuntos)
            .addGroup(panLateralLayout.createSequentialGroup()
                .addGroup(panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panLateralLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panLateralLayout.createSequentialGroup()
                                    .addComponent(lat21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, 0)
                                    .addComponent(lat22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, 0)
                                    .addComponent(lat23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, 0)
                                    .addComponent(lat24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(panLateralLayout.createSequentialGroup()
                                    .addComponent(lat17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, 0)
                                    .addComponent(lat18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, 0)
                                    .addComponent(let20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, 0)
                                    .addComponent(let19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panLateralLayout.createSequentialGroup()
                                    .addComponent(lat9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, 0)
                                    .addComponent(lat10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, 0)
                                    .addComponent(let12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, 0)
                                    .addComponent(let11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(panLateralLayout.createSequentialGroup()
                                    .addComponent(lat13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, 0)
                                    .addComponent(lat14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, 0)
                                    .addComponent(lat15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, 0)
                                    .addComponent(lat16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panLateralLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panLateralLayout.createSequentialGroup()
                        .addComponent(lat5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(lat6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(lat7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(lat8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panLateralLayout.createSequentialGroup()
                        .addComponent(lat1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(lat2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(let4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(let3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12))
        );
        panLateralLayout.setVerticalGroup(
            panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panLateralLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel3)
                .addGap(12, 12, 12)
                .addGroup(panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lat1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lat2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(let3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(let4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addGroup(panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lat5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lat6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lat7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lat8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lat9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lat10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(let11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(let12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addGroup(panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lat13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lat14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lat15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lat16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lat17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lat18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(let19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(let20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addGroup(panLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lat21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lat22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lat23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lat24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(6, 6, 6)
                .addComponent(txtPuntos, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );

        e1.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e1Layout = new javax.swing.GroupLayout(e1);
        e1.setLayout(e1Layout);
        e1Layout.setHorizontalGroup(
            e1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e1Layout.setVerticalGroup(
            e1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e2.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e2Layout = new javax.swing.GroupLayout(e2);
        e2.setLayout(e2Layout);
        e2Layout.setHorizontalGroup(
            e2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e2Layout.setVerticalGroup(
            e2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e3.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e3Layout = new javax.swing.GroupLayout(e3);
        e3.setLayout(e3Layout);
        e3Layout.setHorizontalGroup(
            e3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e3Layout.setVerticalGroup(
            e3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e4.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e4Layout = new javax.swing.GroupLayout(e4);
        e4.setLayout(e4Layout);
        e4Layout.setHorizontalGroup(
            e4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e4Layout.setVerticalGroup(
            e4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e5.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e5Layout = new javax.swing.GroupLayout(e5);
        e5.setLayout(e5Layout);
        e5Layout.setHorizontalGroup(
            e5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e5Layout.setVerticalGroup(
            e5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e6.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e6Layout = new javax.swing.GroupLayout(e6);
        e6.setLayout(e6Layout);
        e6Layout.setHorizontalGroup(
            e6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e6Layout.setVerticalGroup(
            e6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e7.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e7Layout = new javax.swing.GroupLayout(e7);
        e7.setLayout(e7Layout);
        e7Layout.setHorizontalGroup(
            e7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e7Layout.setVerticalGroup(
            e7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e8.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e8Layout = new javax.swing.GroupLayout(e8);
        e8.setLayout(e8Layout);
        e8Layout.setHorizontalGroup(
            e8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e8Layout.setVerticalGroup(
            e8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e9.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e9Layout = new javax.swing.GroupLayout(e9);
        e9.setLayout(e9Layout);
        e9Layout.setHorizontalGroup(
            e9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e9Layout.setVerticalGroup(
            e9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e10.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e10Layout = new javax.swing.GroupLayout(e10);
        e10.setLayout(e10Layout);
        e10Layout.setHorizontalGroup(
            e10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e10Layout.setVerticalGroup(
            e10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e11.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e11Layout = new javax.swing.GroupLayout(e11);
        e11.setLayout(e11Layout);
        e11Layout.setHorizontalGroup(
            e11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e11Layout.setVerticalGroup(
            e11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e12.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e12Layout = new javax.swing.GroupLayout(e12);
        e12.setLayout(e12Layout);
        e12Layout.setHorizontalGroup(
            e12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e12Layout.setVerticalGroup(
            e12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e13.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e13Layout = new javax.swing.GroupLayout(e13);
        e13.setLayout(e13Layout);
        e13Layout.setHorizontalGroup(
            e13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e13Layout.setVerticalGroup(
            e13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e14.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e14Layout = new javax.swing.GroupLayout(e14);
        e14.setLayout(e14Layout);
        e14Layout.setHorizontalGroup(
            e14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e14Layout.setVerticalGroup(
            e14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e15.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e15Layout = new javax.swing.GroupLayout(e15);
        e15.setLayout(e15Layout);
        e15Layout.setHorizontalGroup(
            e15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e15Layout.setVerticalGroup(
            e15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e16.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e16Layout = new javax.swing.GroupLayout(e16);
        e16.setLayout(e16Layout);
        e16Layout.setHorizontalGroup(
            e16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e16Layout.setVerticalGroup(
            e16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e17.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e17Layout = new javax.swing.GroupLayout(e17);
        e17.setLayout(e17Layout);
        e17Layout.setHorizontalGroup(
            e17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e17Layout.setVerticalGroup(
            e17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e18.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e18Layout = new javax.swing.GroupLayout(e18);
        e18.setLayout(e18Layout);
        e18Layout.setHorizontalGroup(
            e18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e18Layout.setVerticalGroup(
            e18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e19.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e19Layout = new javax.swing.GroupLayout(e19);
        e19.setLayout(e19Layout);
        e19Layout.setHorizontalGroup(
            e19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e19Layout.setVerticalGroup(
            e19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e20.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e20Layout = new javax.swing.GroupLayout(e20);
        e20.setLayout(e20Layout);
        e20Layout.setHorizontalGroup(
            e20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e20Layout.setVerticalGroup(
            e20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e21.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e21Layout = new javax.swing.GroupLayout(e21);
        e21.setLayout(e21Layout);
        e21Layout.setHorizontalGroup(
            e21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e21Layout.setVerticalGroup(
            e21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e22.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e22Layout = new javax.swing.GroupLayout(e22);
        e22.setLayout(e22Layout);
        e22Layout.setHorizontalGroup(
            e22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e22Layout.setVerticalGroup(
            e22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e23.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e23Layout = new javax.swing.GroupLayout(e23);
        e23.setLayout(e23Layout);
        e23Layout.setHorizontalGroup(
            e23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e23Layout.setVerticalGroup(
            e23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e24.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e24Layout = new javax.swing.GroupLayout(e24);
        e24.setLayout(e24Layout);
        e24Layout.setHorizontalGroup(
            e24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e24Layout.setVerticalGroup(
            e24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e25.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e25Layout = new javax.swing.GroupLayout(e25);
        e25.setLayout(e25Layout);
        e25Layout.setHorizontalGroup(
            e25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e25Layout.setVerticalGroup(
            e25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e26.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e26Layout = new javax.swing.GroupLayout(e26);
        e26.setLayout(e26Layout);
        e26Layout.setHorizontalGroup(
            e26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e26Layout.setVerticalGroup(
            e26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e27.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e27Layout = new javax.swing.GroupLayout(e27);
        e27.setLayout(e27Layout);
        e27Layout.setHorizontalGroup(
            e27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e27Layout.setVerticalGroup(
            e27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e28.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e28Layout = new javax.swing.GroupLayout(e28);
        e28.setLayout(e28Layout);
        e28Layout.setHorizontalGroup(
            e28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e28Layout.setVerticalGroup(
            e28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e29.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e29Layout = new javax.swing.GroupLayout(e29);
        e29.setLayout(e29Layout);
        e29Layout.setHorizontalGroup(
            e29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e29Layout.setVerticalGroup(
            e29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e30.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e30Layout = new javax.swing.GroupLayout(e30);
        e30.setLayout(e30Layout);
        e30Layout.setHorizontalGroup(
            e30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e30Layout.setVerticalGroup(
            e30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e31.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e31Layout = new javax.swing.GroupLayout(e31);
        e31.setLayout(e31Layout);
        e31Layout.setHorizontalGroup(
            e31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e31Layout.setVerticalGroup(
            e31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e32.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e32Layout = new javax.swing.GroupLayout(e32);
        e32.setLayout(e32Layout);
        e32Layout.setHorizontalGroup(
            e32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e32Layout.setVerticalGroup(
            e32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e33.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e33Layout = new javax.swing.GroupLayout(e33);
        e33.setLayout(e33Layout);
        e33Layout.setHorizontalGroup(
            e33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e33Layout.setVerticalGroup(
            e33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e34.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e34Layout = new javax.swing.GroupLayout(e34);
        e34.setLayout(e34Layout);
        e34Layout.setHorizontalGroup(
            e34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e34Layout.setVerticalGroup(
            e34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e35.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e35Layout = new javax.swing.GroupLayout(e35);
        e35.setLayout(e35Layout);
        e35Layout.setHorizontalGroup(
            e35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e35Layout.setVerticalGroup(
            e35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e36.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e36Layout = new javax.swing.GroupLayout(e36);
        e36.setLayout(e36Layout);
        e36Layout.setHorizontalGroup(
            e36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e36Layout.setVerticalGroup(
            e36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e37.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e37Layout = new javax.swing.GroupLayout(e37);
        e37.setLayout(e37Layout);
        e37Layout.setHorizontalGroup(
            e37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e37Layout.setVerticalGroup(
            e37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e38.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e38Layout = new javax.swing.GroupLayout(e38);
        e38.setLayout(e38Layout);
        e38Layout.setHorizontalGroup(
            e38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e38Layout.setVerticalGroup(
            e38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e39.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e39Layout = new javax.swing.GroupLayout(e39);
        e39.setLayout(e39Layout);
        e39Layout.setHorizontalGroup(
            e39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e39Layout.setVerticalGroup(
            e39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e40.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e40Layout = new javax.swing.GroupLayout(e40);
        e40.setLayout(e40Layout);
        e40Layout.setHorizontalGroup(
            e40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e40Layout.setVerticalGroup(
            e40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e41.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e41Layout = new javax.swing.GroupLayout(e41);
        e41.setLayout(e41Layout);
        e41Layout.setHorizontalGroup(
            e41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e41Layout.setVerticalGroup(
            e41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e42.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e42Layout = new javax.swing.GroupLayout(e42);
        e42.setLayout(e42Layout);
        e42Layout.setHorizontalGroup(
            e42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e42Layout.setVerticalGroup(
            e42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e43.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e43Layout = new javax.swing.GroupLayout(e43);
        e43.setLayout(e43Layout);
        e43Layout.setHorizontalGroup(
            e43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e43Layout.setVerticalGroup(
            e43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e44.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e44Layout = new javax.swing.GroupLayout(e44);
        e44.setLayout(e44Layout);
        e44Layout.setHorizontalGroup(
            e44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e44Layout.setVerticalGroup(
            e44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e45.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e45Layout = new javax.swing.GroupLayout(e45);
        e45.setLayout(e45Layout);
        e45Layout.setHorizontalGroup(
            e45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e45Layout.setVerticalGroup(
            e45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e46.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e46Layout = new javax.swing.GroupLayout(e46);
        e46.setLayout(e46Layout);
        e46Layout.setHorizontalGroup(
            e46Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e46Layout.setVerticalGroup(
            e46Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e47.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e47Layout = new javax.swing.GroupLayout(e47);
        e47.setLayout(e47Layout);
        e47Layout.setHorizontalGroup(
            e47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e47Layout.setVerticalGroup(
            e47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e48.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e48Layout = new javax.swing.GroupLayout(e48);
        e48.setLayout(e48Layout);
        e48Layout.setHorizontalGroup(
            e48Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e48Layout.setVerticalGroup(
            e48Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e49.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e49Layout = new javax.swing.GroupLayout(e49);
        e49.setLayout(e49Layout);
        e49Layout.setHorizontalGroup(
            e49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e49Layout.setVerticalGroup(
            e49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e50.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e50Layout = new javax.swing.GroupLayout(e50);
        e50.setLayout(e50Layout);
        e50Layout.setHorizontalGroup(
            e50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e50Layout.setVerticalGroup(
            e50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e51.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e51Layout = new javax.swing.GroupLayout(e51);
        e51.setLayout(e51Layout);
        e51Layout.setHorizontalGroup(
            e51Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e51Layout.setVerticalGroup(
            e51Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e52.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e52Layout = new javax.swing.GroupLayout(e52);
        e52.setLayout(e52Layout);
        e52Layout.setHorizontalGroup(
            e52Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e52Layout.setVerticalGroup(
            e52Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e53.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e53Layout = new javax.swing.GroupLayout(e53);
        e53.setLayout(e53Layout);
        e53Layout.setHorizontalGroup(
            e53Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e53Layout.setVerticalGroup(
            e53Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e54.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e54Layout = new javax.swing.GroupLayout(e54);
        e54.setLayout(e54Layout);
        e54Layout.setHorizontalGroup(
            e54Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e54Layout.setVerticalGroup(
            e54Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e55.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e55Layout = new javax.swing.GroupLayout(e55);
        e55.setLayout(e55Layout);
        e55Layout.setHorizontalGroup(
            e55Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e55Layout.setVerticalGroup(
            e55Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e56.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e56Layout = new javax.swing.GroupLayout(e56);
        e56.setLayout(e56Layout);
        e56Layout.setHorizontalGroup(
            e56Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e56Layout.setVerticalGroup(
            e56Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e57.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e57Layout = new javax.swing.GroupLayout(e57);
        e57.setLayout(e57Layout);
        e57Layout.setHorizontalGroup(
            e57Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e57Layout.setVerticalGroup(
            e57Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e58.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e58Layout = new javax.swing.GroupLayout(e58);
        e58.setLayout(e58Layout);
        e58Layout.setHorizontalGroup(
            e58Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e58Layout.setVerticalGroup(
            e58Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e59.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e59Layout = new javax.swing.GroupLayout(e59);
        e59.setLayout(e59Layout);
        e59Layout.setHorizontalGroup(
            e59Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e59Layout.setVerticalGroup(
            e59Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e60.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e60Layout = new javax.swing.GroupLayout(e60);
        e60.setLayout(e60Layout);
        e60Layout.setHorizontalGroup(
            e60Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e60Layout.setVerticalGroup(
            e60Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e61.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e61Layout = new javax.swing.GroupLayout(e61);
        e61.setLayout(e61Layout);
        e61Layout.setHorizontalGroup(
            e61Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e61Layout.setVerticalGroup(
            e61Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e62.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e62Layout = new javax.swing.GroupLayout(e62);
        e62.setLayout(e62Layout);
        e62Layout.setHorizontalGroup(
            e62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e62Layout.setVerticalGroup(
            e62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e63.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e63Layout = new javax.swing.GroupLayout(e63);
        e63.setLayout(e63Layout);
        e63Layout.setHorizontalGroup(
            e63Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e63Layout.setVerticalGroup(
            e63Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e64.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e64Layout = new javax.swing.GroupLayout(e64);
        e64.setLayout(e64Layout);
        e64Layout.setHorizontalGroup(
            e64Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e64Layout.setVerticalGroup(
            e64Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e65.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e65Layout = new javax.swing.GroupLayout(e65);
        e65.setLayout(e65Layout);
        e65Layout.setHorizontalGroup(
            e65Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e65Layout.setVerticalGroup(
            e65Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e66.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e66Layout = new javax.swing.GroupLayout(e66);
        e66.setLayout(e66Layout);
        e66Layout.setHorizontalGroup(
            e66Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e66Layout.setVerticalGroup(
            e66Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e67.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e67Layout = new javax.swing.GroupLayout(e67);
        e67.setLayout(e67Layout);
        e67Layout.setHorizontalGroup(
            e67Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e67Layout.setVerticalGroup(
            e67Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e68.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e68Layout = new javax.swing.GroupLayout(e68);
        e68.setLayout(e68Layout);
        e68Layout.setHorizontalGroup(
            e68Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e68Layout.setVerticalGroup(
            e68Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e69.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e69Layout = new javax.swing.GroupLayout(e69);
        e69.setLayout(e69Layout);
        e69Layout.setHorizontalGroup(
            e69Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e69Layout.setVerticalGroup(
            e69Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e70.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e70Layout = new javax.swing.GroupLayout(e70);
        e70.setLayout(e70Layout);
        e70Layout.setHorizontalGroup(
            e70Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e70Layout.setVerticalGroup(
            e70Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e71.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e71Layout = new javax.swing.GroupLayout(e71);
        e71.setLayout(e71Layout);
        e71Layout.setHorizontalGroup(
            e71Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e71Layout.setVerticalGroup(
            e71Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e72.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e72Layout = new javax.swing.GroupLayout(e72);
        e72.setLayout(e72Layout);
        e72Layout.setHorizontalGroup(
            e72Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e72Layout.setVerticalGroup(
            e72Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e73.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e73Layout = new javax.swing.GroupLayout(e73);
        e73.setLayout(e73Layout);
        e73Layout.setHorizontalGroup(
            e73Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e73Layout.setVerticalGroup(
            e73Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e74.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e74Layout = new javax.swing.GroupLayout(e74);
        e74.setLayout(e74Layout);
        e74Layout.setHorizontalGroup(
            e74Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e74Layout.setVerticalGroup(
            e74Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e75.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e75Layout = new javax.swing.GroupLayout(e75);
        e75.setLayout(e75Layout);
        e75Layout.setHorizontalGroup(
            e75Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e75Layout.setVerticalGroup(
            e75Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e76.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e76Layout = new javax.swing.GroupLayout(e76);
        e76.setLayout(e76Layout);
        e76Layout.setHorizontalGroup(
            e76Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e76Layout.setVerticalGroup(
            e76Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e77.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e77Layout = new javax.swing.GroupLayout(e77);
        e77.setLayout(e77Layout);
        e77Layout.setHorizontalGroup(
            e77Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e77Layout.setVerticalGroup(
            e77Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e78.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e78Layout = new javax.swing.GroupLayout(e78);
        e78.setLayout(e78Layout);
        e78Layout.setHorizontalGroup(
            e78Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e78Layout.setVerticalGroup(
            e78Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e79.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e79Layout = new javax.swing.GroupLayout(e79);
        e79.setLayout(e79Layout);
        e79Layout.setHorizontalGroup(
            e79Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e79Layout.setVerticalGroup(
            e79Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e80.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e80Layout = new javax.swing.GroupLayout(e80);
        e80.setLayout(e80Layout);
        e80Layout.setHorizontalGroup(
            e80Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e80Layout.setVerticalGroup(
            e80Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e81.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e81Layout = new javax.swing.GroupLayout(e81);
        e81.setLayout(e81Layout);
        e81Layout.setHorizontalGroup(
            e81Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e81Layout.setVerticalGroup(
            e81Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e82.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e82Layout = new javax.swing.GroupLayout(e82);
        e82.setLayout(e82Layout);
        e82Layout.setHorizontalGroup(
            e82Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e82Layout.setVerticalGroup(
            e82Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e83.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e83Layout = new javax.swing.GroupLayout(e83);
        e83.setLayout(e83Layout);
        e83Layout.setHorizontalGroup(
            e83Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e83Layout.setVerticalGroup(
            e83Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e84.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e84Layout = new javax.swing.GroupLayout(e84);
        e84.setLayout(e84Layout);
        e84Layout.setHorizontalGroup(
            e84Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e84Layout.setVerticalGroup(
            e84Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e85.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e85Layout = new javax.swing.GroupLayout(e85);
        e85.setLayout(e85Layout);
        e85Layout.setHorizontalGroup(
            e85Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e85Layout.setVerticalGroup(
            e85Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e86.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e86Layout = new javax.swing.GroupLayout(e86);
        e86.setLayout(e86Layout);
        e86Layout.setHorizontalGroup(
            e86Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e86Layout.setVerticalGroup(
            e86Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e87.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e87Layout = new javax.swing.GroupLayout(e87);
        e87.setLayout(e87Layout);
        e87Layout.setHorizontalGroup(
            e87Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e87Layout.setVerticalGroup(
            e87Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e88.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e88Layout = new javax.swing.GroupLayout(e88);
        e88.setLayout(e88Layout);
        e88Layout.setHorizontalGroup(
            e88Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e88Layout.setVerticalGroup(
            e88Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e89.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e89Layout = new javax.swing.GroupLayout(e89);
        e89.setLayout(e89Layout);
        e89Layout.setHorizontalGroup(
            e89Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e89Layout.setVerticalGroup(
            e89Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e90.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e90Layout = new javax.swing.GroupLayout(e90);
        e90.setLayout(e90Layout);
        e90Layout.setHorizontalGroup(
            e90Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e90Layout.setVerticalGroup(
            e90Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e91.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e91Layout = new javax.swing.GroupLayout(e91);
        e91.setLayout(e91Layout);
        e91Layout.setHorizontalGroup(
            e91Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e91Layout.setVerticalGroup(
            e91Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e92.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e92Layout = new javax.swing.GroupLayout(e92);
        e92.setLayout(e92Layout);
        e92Layout.setHorizontalGroup(
            e92Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e92Layout.setVerticalGroup(
            e92Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e93.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e93Layout = new javax.swing.GroupLayout(e93);
        e93.setLayout(e93Layout);
        e93Layout.setHorizontalGroup(
            e93Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e93Layout.setVerticalGroup(
            e93Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e94.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e94Layout = new javax.swing.GroupLayout(e94);
        e94.setLayout(e94Layout);
        e94Layout.setHorizontalGroup(
            e94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e94Layout.setVerticalGroup(
            e94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e95.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e95Layout = new javax.swing.GroupLayout(e95);
        e95.setLayout(e95Layout);
        e95Layout.setHorizontalGroup(
            e95Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e95Layout.setVerticalGroup(
            e95Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e96.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e96Layout = new javax.swing.GroupLayout(e96);
        e96.setLayout(e96Layout);
        e96Layout.setHorizontalGroup(
            e96Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e96Layout.setVerticalGroup(
            e96Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e97.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e97Layout = new javax.swing.GroupLayout(e97);
        e97.setLayout(e97Layout);
        e97Layout.setHorizontalGroup(
            e97Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e97Layout.setVerticalGroup(
            e97Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e98.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e98Layout = new javax.swing.GroupLayout(e98);
        e98.setLayout(e98Layout);
        e98Layout.setHorizontalGroup(
            e98Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e98Layout.setVerticalGroup(
            e98Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e99.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e99Layout = new javax.swing.GroupLayout(e99);
        e99.setLayout(e99Layout);
        e99Layout.setHorizontalGroup(
            e99Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e99Layout.setVerticalGroup(
            e99Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e100.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e100Layout = new javax.swing.GroupLayout(e100);
        e100.setLayout(e100Layout);
        e100Layout.setHorizontalGroup(
            e100Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e100Layout.setVerticalGroup(
            e100Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e101.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e101Layout = new javax.swing.GroupLayout(e101);
        e101.setLayout(e101Layout);
        e101Layout.setHorizontalGroup(
            e101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e101Layout.setVerticalGroup(
            e101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e102.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e102Layout = new javax.swing.GroupLayout(e102);
        e102.setLayout(e102Layout);
        e102Layout.setHorizontalGroup(
            e102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e102Layout.setVerticalGroup(
            e102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e103.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e103Layout = new javax.swing.GroupLayout(e103);
        e103.setLayout(e103Layout);
        e103Layout.setHorizontalGroup(
            e103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e103Layout.setVerticalGroup(
            e103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e104.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e104Layout = new javax.swing.GroupLayout(e104);
        e104.setLayout(e104Layout);
        e104Layout.setHorizontalGroup(
            e104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e104Layout.setVerticalGroup(
            e104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e105.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e105Layout = new javax.swing.GroupLayout(e105);
        e105.setLayout(e105Layout);
        e105Layout.setHorizontalGroup(
            e105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e105Layout.setVerticalGroup(
            e105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e106.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e106Layout = new javax.swing.GroupLayout(e106);
        e106.setLayout(e106Layout);
        e106Layout.setHorizontalGroup(
            e106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e106Layout.setVerticalGroup(
            e106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e107.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e107Layout = new javax.swing.GroupLayout(e107);
        e107.setLayout(e107Layout);
        e107Layout.setHorizontalGroup(
            e107Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e107Layout.setVerticalGroup(
            e107Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e108.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e108Layout = new javax.swing.GroupLayout(e108);
        e108.setLayout(e108Layout);
        e108Layout.setHorizontalGroup(
            e108Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e108Layout.setVerticalGroup(
            e108Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e109.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e109Layout = new javax.swing.GroupLayout(e109);
        e109.setLayout(e109Layout);
        e109Layout.setHorizontalGroup(
            e109Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e109Layout.setVerticalGroup(
            e109Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e110.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e110Layout = new javax.swing.GroupLayout(e110);
        e110.setLayout(e110Layout);
        e110Layout.setHorizontalGroup(
            e110Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e110Layout.setVerticalGroup(
            e110Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e111.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e111Layout = new javax.swing.GroupLayout(e111);
        e111.setLayout(e111Layout);
        e111Layout.setHorizontalGroup(
            e111Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e111Layout.setVerticalGroup(
            e111Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e112.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e112Layout = new javax.swing.GroupLayout(e112);
        e112.setLayout(e112Layout);
        e112Layout.setHorizontalGroup(
            e112Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e112Layout.setVerticalGroup(
            e112Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e113.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e113Layout = new javax.swing.GroupLayout(e113);
        e113.setLayout(e113Layout);
        e113Layout.setHorizontalGroup(
            e113Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e113Layout.setVerticalGroup(
            e113Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e114.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e114Layout = new javax.swing.GroupLayout(e114);
        e114.setLayout(e114Layout);
        e114Layout.setHorizontalGroup(
            e114Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e114Layout.setVerticalGroup(
            e114Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e115.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e115Layout = new javax.swing.GroupLayout(e115);
        e115.setLayout(e115Layout);
        e115Layout.setHorizontalGroup(
            e115Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e115Layout.setVerticalGroup(
            e115Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e116.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e116Layout = new javax.swing.GroupLayout(e116);
        e116.setLayout(e116Layout);
        e116Layout.setHorizontalGroup(
            e116Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e116Layout.setVerticalGroup(
            e116Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e117.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e117Layout = new javax.swing.GroupLayout(e117);
        e117.setLayout(e117Layout);
        e117Layout.setHorizontalGroup(
            e117Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e117Layout.setVerticalGroup(
            e117Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e118.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e118Layout = new javax.swing.GroupLayout(e118);
        e118.setLayout(e118Layout);
        e118Layout.setHorizontalGroup(
            e118Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e118Layout.setVerticalGroup(
            e118Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e119.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e119Layout = new javax.swing.GroupLayout(e119);
        e119.setLayout(e119Layout);
        e119Layout.setHorizontalGroup(
            e119Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e119Layout.setVerticalGroup(
            e119Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e120.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e120Layout = new javax.swing.GroupLayout(e120);
        e120.setLayout(e120Layout);
        e120Layout.setHorizontalGroup(
            e120Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e120Layout.setVerticalGroup(
            e120Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e121.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e121Layout = new javax.swing.GroupLayout(e121);
        e121.setLayout(e121Layout);
        e121Layout.setHorizontalGroup(
            e121Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e121Layout.setVerticalGroup(
            e121Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e122.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e122Layout = new javax.swing.GroupLayout(e122);
        e122.setLayout(e122Layout);
        e122Layout.setHorizontalGroup(
            e122Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e122Layout.setVerticalGroup(
            e122Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e123.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e123Layout = new javax.swing.GroupLayout(e123);
        e123.setLayout(e123Layout);
        e123Layout.setHorizontalGroup(
            e123Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e123Layout.setVerticalGroup(
            e123Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e124.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e124Layout = new javax.swing.GroupLayout(e124);
        e124.setLayout(e124Layout);
        e124Layout.setHorizontalGroup(
            e124Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e124Layout.setVerticalGroup(
            e124Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e125.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e125Layout = new javax.swing.GroupLayout(e125);
        e125.setLayout(e125Layout);
        e125Layout.setHorizontalGroup(
            e125Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e125Layout.setVerticalGroup(
            e125Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e126.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e126Layout = new javax.swing.GroupLayout(e126);
        e126.setLayout(e126Layout);
        e126Layout.setHorizontalGroup(
            e126Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e126Layout.setVerticalGroup(
            e126Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e127.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e127Layout = new javax.swing.GroupLayout(e127);
        e127.setLayout(e127Layout);
        e127Layout.setHorizontalGroup(
            e127Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e127Layout.setVerticalGroup(
            e127Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e128.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e128Layout = new javax.swing.GroupLayout(e128);
        e128.setLayout(e128Layout);
        e128Layout.setHorizontalGroup(
            e128Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e128Layout.setVerticalGroup(
            e128Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e129.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e129Layout = new javax.swing.GroupLayout(e129);
        e129.setLayout(e129Layout);
        e129Layout.setHorizontalGroup(
            e129Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e129Layout.setVerticalGroup(
            e129Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e130.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e130Layout = new javax.swing.GroupLayout(e130);
        e130.setLayout(e130Layout);
        e130Layout.setHorizontalGroup(
            e130Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e130Layout.setVerticalGroup(
            e130Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e131.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e131Layout = new javax.swing.GroupLayout(e131);
        e131.setLayout(e131Layout);
        e131Layout.setHorizontalGroup(
            e131Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e131Layout.setVerticalGroup(
            e131Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e132.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e132Layout = new javax.swing.GroupLayout(e132);
        e132.setLayout(e132Layout);
        e132Layout.setHorizontalGroup(
            e132Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e132Layout.setVerticalGroup(
            e132Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e133.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e133Layout = new javax.swing.GroupLayout(e133);
        e133.setLayout(e133Layout);
        e133Layout.setHorizontalGroup(
            e133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e133Layout.setVerticalGroup(
            e133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e134.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e134Layout = new javax.swing.GroupLayout(e134);
        e134.setLayout(e134Layout);
        e134Layout.setHorizontalGroup(
            e134Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e134Layout.setVerticalGroup(
            e134Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e135.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e135Layout = new javax.swing.GroupLayout(e135);
        e135.setLayout(e135Layout);
        e135Layout.setHorizontalGroup(
            e135Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e135Layout.setVerticalGroup(
            e135Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e136.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e136Layout = new javax.swing.GroupLayout(e136);
        e136.setLayout(e136Layout);
        e136Layout.setHorizontalGroup(
            e136Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e136Layout.setVerticalGroup(
            e136Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e137.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e137Layout = new javax.swing.GroupLayout(e137);
        e137.setLayout(e137Layout);
        e137Layout.setHorizontalGroup(
            e137Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e137Layout.setVerticalGroup(
            e137Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e138.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e138Layout = new javax.swing.GroupLayout(e138);
        e138.setLayout(e138Layout);
        e138Layout.setHorizontalGroup(
            e138Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e138Layout.setVerticalGroup(
            e138Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e139.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e139Layout = new javax.swing.GroupLayout(e139);
        e139.setLayout(e139Layout);
        e139Layout.setHorizontalGroup(
            e139Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e139Layout.setVerticalGroup(
            e139Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e140.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e140Layout = new javax.swing.GroupLayout(e140);
        e140.setLayout(e140Layout);
        e140Layout.setHorizontalGroup(
            e140Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e140Layout.setVerticalGroup(
            e140Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e141.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e141Layout = new javax.swing.GroupLayout(e141);
        e141.setLayout(e141Layout);
        e141Layout.setHorizontalGroup(
            e141Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e141Layout.setVerticalGroup(
            e141Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e142.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e142Layout = new javax.swing.GroupLayout(e142);
        e142.setLayout(e142Layout);
        e142Layout.setHorizontalGroup(
            e142Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e142Layout.setVerticalGroup(
            e142Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e143.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e143Layout = new javax.swing.GroupLayout(e143);
        e143.setLayout(e143Layout);
        e143Layout.setHorizontalGroup(
            e143Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e143Layout.setVerticalGroup(
            e143Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e144.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e144Layout = new javax.swing.GroupLayout(e144);
        e144.setLayout(e144Layout);
        e144Layout.setHorizontalGroup(
            e144Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e144Layout.setVerticalGroup(
            e144Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e145.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e145Layout = new javax.swing.GroupLayout(e145);
        e145.setLayout(e145Layout);
        e145Layout.setHorizontalGroup(
            e145Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e145Layout.setVerticalGroup(
            e145Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e146.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e146Layout = new javax.swing.GroupLayout(e146);
        e146.setLayout(e146Layout);
        e146Layout.setHorizontalGroup(
            e146Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e146Layout.setVerticalGroup(
            e146Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e147.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e147Layout = new javax.swing.GroupLayout(e147);
        e147.setLayout(e147Layout);
        e147Layout.setHorizontalGroup(
            e147Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e147Layout.setVerticalGroup(
            e147Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e148.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e148Layout = new javax.swing.GroupLayout(e148);
        e148.setLayout(e148Layout);
        e148Layout.setHorizontalGroup(
            e148Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e148Layout.setVerticalGroup(
            e148Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e149.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e149Layout = new javax.swing.GroupLayout(e149);
        e149.setLayout(e149Layout);
        e149Layout.setHorizontalGroup(
            e149Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e149Layout.setVerticalGroup(
            e149Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e150.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e150Layout = new javax.swing.GroupLayout(e150);
        e150.setLayout(e150Layout);
        e150Layout.setHorizontalGroup(
            e150Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e150Layout.setVerticalGroup(
            e150Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e151.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e151Layout = new javax.swing.GroupLayout(e151);
        e151.setLayout(e151Layout);
        e151Layout.setHorizontalGroup(
            e151Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e151Layout.setVerticalGroup(
            e151Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e152.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e152Layout = new javax.swing.GroupLayout(e152);
        e152.setLayout(e152Layout);
        e152Layout.setHorizontalGroup(
            e152Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e152Layout.setVerticalGroup(
            e152Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e153.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e153Layout = new javax.swing.GroupLayout(e153);
        e153.setLayout(e153Layout);
        e153Layout.setHorizontalGroup(
            e153Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e153Layout.setVerticalGroup(
            e153Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e154.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e154Layout = new javax.swing.GroupLayout(e154);
        e154.setLayout(e154Layout);
        e154Layout.setHorizontalGroup(
            e154Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e154Layout.setVerticalGroup(
            e154Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e155.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e155Layout = new javax.swing.GroupLayout(e155);
        e155.setLayout(e155Layout);
        e155Layout.setHorizontalGroup(
            e155Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e155Layout.setVerticalGroup(
            e155Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e156.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e156Layout = new javax.swing.GroupLayout(e156);
        e156.setLayout(e156Layout);
        e156Layout.setHorizontalGroup(
            e156Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e156Layout.setVerticalGroup(
            e156Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e157.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e157Layout = new javax.swing.GroupLayout(e157);
        e157.setLayout(e157Layout);
        e157Layout.setHorizontalGroup(
            e157Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e157Layout.setVerticalGroup(
            e157Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e158.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e158Layout = new javax.swing.GroupLayout(e158);
        e158.setLayout(e158Layout);
        e158Layout.setHorizontalGroup(
            e158Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e158Layout.setVerticalGroup(
            e158Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e159.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e159Layout = new javax.swing.GroupLayout(e159);
        e159.setLayout(e159Layout);
        e159Layout.setHorizontalGroup(
            e159Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e159Layout.setVerticalGroup(
            e159Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e160.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e160Layout = new javax.swing.GroupLayout(e160);
        e160.setLayout(e160Layout);
        e160Layout.setHorizontalGroup(
            e160Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e160Layout.setVerticalGroup(
            e160Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e161.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e161Layout = new javax.swing.GroupLayout(e161);
        e161.setLayout(e161Layout);
        e161Layout.setHorizontalGroup(
            e161Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e161Layout.setVerticalGroup(
            e161Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e162.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e162Layout = new javax.swing.GroupLayout(e162);
        e162.setLayout(e162Layout);
        e162Layout.setHorizontalGroup(
            e162Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e162Layout.setVerticalGroup(
            e162Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e163.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e163Layout = new javax.swing.GroupLayout(e163);
        e163.setLayout(e163Layout);
        e163Layout.setHorizontalGroup(
            e163Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e163Layout.setVerticalGroup(
            e163Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e164.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e164Layout = new javax.swing.GroupLayout(e164);
        e164.setLayout(e164Layout);
        e164Layout.setHorizontalGroup(
            e164Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e164Layout.setVerticalGroup(
            e164Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e165.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e165Layout = new javax.swing.GroupLayout(e165);
        e165.setLayout(e165Layout);
        e165Layout.setHorizontalGroup(
            e165Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e165Layout.setVerticalGroup(
            e165Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e166.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e166Layout = new javax.swing.GroupLayout(e166);
        e166.setLayout(e166Layout);
        e166Layout.setHorizontalGroup(
            e166Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e166Layout.setVerticalGroup(
            e166Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e167.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e167Layout = new javax.swing.GroupLayout(e167);
        e167.setLayout(e167Layout);
        e167Layout.setHorizontalGroup(
            e167Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e167Layout.setVerticalGroup(
            e167Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e168.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e168Layout = new javax.swing.GroupLayout(e168);
        e168.setLayout(e168Layout);
        e168Layout.setHorizontalGroup(
            e168Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e168Layout.setVerticalGroup(
            e168Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e169.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e169Layout = new javax.swing.GroupLayout(e169);
        e169.setLayout(e169Layout);
        e169Layout.setHorizontalGroup(
            e169Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e169Layout.setVerticalGroup(
            e169Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e170.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e170Layout = new javax.swing.GroupLayout(e170);
        e170.setLayout(e170Layout);
        e170Layout.setHorizontalGroup(
            e170Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e170Layout.setVerticalGroup(
            e170Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e171.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e171Layout = new javax.swing.GroupLayout(e171);
        e171.setLayout(e171Layout);
        e171Layout.setHorizontalGroup(
            e171Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e171Layout.setVerticalGroup(
            e171Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e172.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e172Layout = new javax.swing.GroupLayout(e172);
        e172.setLayout(e172Layout);
        e172Layout.setHorizontalGroup(
            e172Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e172Layout.setVerticalGroup(
            e172Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e173.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e173Layout = new javax.swing.GroupLayout(e173);
        e173.setLayout(e173Layout);
        e173Layout.setHorizontalGroup(
            e173Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e173Layout.setVerticalGroup(
            e173Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e174.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e174Layout = new javax.swing.GroupLayout(e174);
        e174.setLayout(e174Layout);
        e174Layout.setHorizontalGroup(
            e174Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e174Layout.setVerticalGroup(
            e174Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e175.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e175Layout = new javax.swing.GroupLayout(e175);
        e175.setLayout(e175Layout);
        e175Layout.setHorizontalGroup(
            e175Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e175Layout.setVerticalGroup(
            e175Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e176.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e176Layout = new javax.swing.GroupLayout(e176);
        e176.setLayout(e176Layout);
        e176Layout.setHorizontalGroup(
            e176Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e176Layout.setVerticalGroup(
            e176Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e177.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e177Layout = new javax.swing.GroupLayout(e177);
        e177.setLayout(e177Layout);
        e177Layout.setHorizontalGroup(
            e177Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e177Layout.setVerticalGroup(
            e177Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e178.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e178Layout = new javax.swing.GroupLayout(e178);
        e178.setLayout(e178Layout);
        e178Layout.setHorizontalGroup(
            e178Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e178Layout.setVerticalGroup(
            e178Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e179.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout e179Layout = new javax.swing.GroupLayout(e179);
        e179.setLayout(e179Layout);
        e179Layout.setHorizontalGroup(
            e179Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e179Layout.setVerticalGroup(
            e179Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e180.setBackground(new java.awt.Color(0, 0, 0));
        e180.setPreferredSize(new java.awt.Dimension(33, 33));

        javax.swing.GroupLayout e180Layout = new javax.swing.GroupLayout(e180);
        e180.setLayout(e180Layout);
        e180Layout.setHorizontalGroup(
            e180Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );
        e180Layout.setVerticalGroup(
            e180Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 33, Short.MAX_VALUE)
        );

        e182.setBackground(new java.awt.Color(103, 91, 63));
        e182.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e182Layout = new javax.swing.GroupLayout(e182);
        e182.setLayout(e182Layout);
        e182Layout.setHorizontalGroup(
            e182Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e182Layout.setVerticalGroup(
            e182Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e183.setBackground(new java.awt.Color(103, 91, 63));
        e183.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e183Layout = new javax.swing.GroupLayout(e183);
        e183.setLayout(e183Layout);
        e183Layout.setHorizontalGroup(
            e183Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e183Layout.setVerticalGroup(
            e183Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e187.setBackground(new java.awt.Color(103, 91, 63));
        e187.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e187Layout = new javax.swing.GroupLayout(e187);
        e187.setLayout(e187Layout);
        e187Layout.setHorizontalGroup(
            e187Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e187Layout.setVerticalGroup(
            e187Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e188.setBackground(new java.awt.Color(103, 91, 63));
        e188.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e188Layout = new javax.swing.GroupLayout(e188);
        e188.setLayout(e188Layout);
        e188Layout.setHorizontalGroup(
            e188Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e188Layout.setVerticalGroup(
            e188Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e189.setBackground(new java.awt.Color(103, 91, 63));
        e189.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e189Layout = new javax.swing.GroupLayout(e189);
        e189.setLayout(e189Layout);
        e189Layout.setHorizontalGroup(
            e189Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e189Layout.setVerticalGroup(
            e189Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e190.setBackground(new java.awt.Color(103, 91, 63));
        e190.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e190Layout = new javax.swing.GroupLayout(e190);
        e190.setLayout(e190Layout);
        e190Layout.setHorizontalGroup(
            e190Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e190Layout.setVerticalGroup(
            e190Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e191.setBackground(new java.awt.Color(103, 91, 63));
        e191.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e191Layout = new javax.swing.GroupLayout(e191);
        e191.setLayout(e191Layout);
        e191Layout.setHorizontalGroup(
            e191Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e191Layout.setVerticalGroup(
            e191Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e192.setBackground(new java.awt.Color(103, 91, 63));
        e192.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e192Layout = new javax.swing.GroupLayout(e192);
        e192.setLayout(e192Layout);
        e192Layout.setHorizontalGroup(
            e192Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e192Layout.setVerticalGroup(
            e192Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e193.setBackground(new java.awt.Color(103, 91, 63));
        e193.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e193Layout = new javax.swing.GroupLayout(e193);
        e193.setLayout(e193Layout);
        e193Layout.setHorizontalGroup(
            e193Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e193Layout.setVerticalGroup(
            e193Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e194.setBackground(new java.awt.Color(103, 91, 63));
        e194.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e194Layout = new javax.swing.GroupLayout(e194);
        e194.setLayout(e194Layout);
        e194Layout.setHorizontalGroup(
            e194Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e194Layout.setVerticalGroup(
            e194Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e195.setBackground(new java.awt.Color(103, 91, 63));
        e195.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e195Layout = new javax.swing.GroupLayout(e195);
        e195.setLayout(e195Layout);
        e195Layout.setHorizontalGroup(
            e195Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e195Layout.setVerticalGroup(
            e195Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e196.setBackground(new java.awt.Color(103, 91, 63));
        e196.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e196Layout = new javax.swing.GroupLayout(e196);
        e196.setLayout(e196Layout);
        e196Layout.setHorizontalGroup(
            e196Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e196Layout.setVerticalGroup(
            e196Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e197.setBackground(new java.awt.Color(103, 91, 63));
        e197.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e197Layout = new javax.swing.GroupLayout(e197);
        e197.setLayout(e197Layout);
        e197Layout.setHorizontalGroup(
            e197Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e197Layout.setVerticalGroup(
            e197Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e198.setBackground(new java.awt.Color(103, 91, 63));
        e198.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e198Layout = new javax.swing.GroupLayout(e198);
        e198.setLayout(e198Layout);
        e198Layout.setHorizontalGroup(
            e198Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e198Layout.setVerticalGroup(
            e198Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e199.setBackground(new java.awt.Color(103, 91, 63));
        e199.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e199Layout = new javax.swing.GroupLayout(e199);
        e199.setLayout(e199Layout);
        e199Layout.setHorizontalGroup(
            e199Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e199Layout.setVerticalGroup(
            e199Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e200.setBackground(new java.awt.Color(103, 91, 63));
        e200.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e200Layout = new javax.swing.GroupLayout(e200);
        e200.setLayout(e200Layout);
        e200Layout.setHorizontalGroup(
            e200Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e200Layout.setVerticalGroup(
            e200Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e201.setBackground(new java.awt.Color(103, 91, 63));
        e201.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e201Layout = new javax.swing.GroupLayout(e201);
        e201.setLayout(e201Layout);
        e201Layout.setHorizontalGroup(
            e201Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e201Layout.setVerticalGroup(
            e201Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e202.setBackground(new java.awt.Color(103, 91, 63));
        e202.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e202Layout = new javax.swing.GroupLayout(e202);
        e202.setLayout(e202Layout);
        e202Layout.setHorizontalGroup(
            e202Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e202Layout.setVerticalGroup(
            e202Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e203.setBackground(new java.awt.Color(103, 91, 63));
        e203.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e203Layout = new javax.swing.GroupLayout(e203);
        e203.setLayout(e203Layout);
        e203Layout.setHorizontalGroup(
            e203Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e203Layout.setVerticalGroup(
            e203Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e204.setBackground(new java.awt.Color(103, 91, 63));
        e204.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e204Layout = new javax.swing.GroupLayout(e204);
        e204.setLayout(e204Layout);
        e204Layout.setHorizontalGroup(
            e204Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e204Layout.setVerticalGroup(
            e204Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e225.setBackground(new java.awt.Color(103, 91, 63));
        e225.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e225Layout = new javax.swing.GroupLayout(e225);
        e225.setLayout(e225Layout);
        e225Layout.setHorizontalGroup(
            e225Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e225Layout.setVerticalGroup(
            e225Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e226.setBackground(new java.awt.Color(103, 91, 63));
        e226.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e226Layout = new javax.swing.GroupLayout(e226);
        e226.setLayout(e226Layout);
        e226Layout.setHorizontalGroup(
            e226Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e226Layout.setVerticalGroup(
            e226Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e227.setBackground(new java.awt.Color(103, 91, 63));
        e227.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e227Layout = new javax.swing.GroupLayout(e227);
        e227.setLayout(e227Layout);
        e227Layout.setHorizontalGroup(
            e227Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e227Layout.setVerticalGroup(
            e227Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e228.setBackground(new java.awt.Color(103, 91, 63));
        e228.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e228Layout = new javax.swing.GroupLayout(e228);
        e228.setLayout(e228Layout);
        e228Layout.setHorizontalGroup(
            e228Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e228Layout.setVerticalGroup(
            e228Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e229.setBackground(new java.awt.Color(103, 91, 63));
        e229.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e229Layout = new javax.swing.GroupLayout(e229);
        e229.setLayout(e229Layout);
        e229Layout.setHorizontalGroup(
            e229Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e229Layout.setVerticalGroup(
            e229Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e230.setBackground(new java.awt.Color(103, 91, 63));
        e230.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e230Layout = new javax.swing.GroupLayout(e230);
        e230.setLayout(e230Layout);
        e230Layout.setHorizontalGroup(
            e230Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e230Layout.setVerticalGroup(
            e230Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e231.setBackground(new java.awt.Color(103, 91, 63));
        e231.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e231Layout = new javax.swing.GroupLayout(e231);
        e231.setLayout(e231Layout);
        e231Layout.setHorizontalGroup(
            e231Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e231Layout.setVerticalGroup(
            e231Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e232.setBackground(new java.awt.Color(103, 91, 63));
        e232.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e232Layout = new javax.swing.GroupLayout(e232);
        e232.setLayout(e232Layout);
        e232Layout.setHorizontalGroup(
            e232Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e232Layout.setVerticalGroup(
            e232Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e233.setBackground(new java.awt.Color(103, 91, 63));
        e233.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e233Layout = new javax.swing.GroupLayout(e233);
        e233.setLayout(e233Layout);
        e233Layout.setHorizontalGroup(
            e233Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e233Layout.setVerticalGroup(
            e233Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e234.setBackground(new java.awt.Color(103, 91, 63));
        e234.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e234Layout = new javax.swing.GroupLayout(e234);
        e234.setLayout(e234Layout);
        e234Layout.setHorizontalGroup(
            e234Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e234Layout.setVerticalGroup(
            e234Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e235.setBackground(new java.awt.Color(103, 91, 63));
        e235.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e235Layout = new javax.swing.GroupLayout(e235);
        e235.setLayout(e235Layout);
        e235Layout.setHorizontalGroup(
            e235Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e235Layout.setVerticalGroup(
            e235Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e236.setBackground(new java.awt.Color(103, 91, 63));
        e236.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e236Layout = new javax.swing.GroupLayout(e236);
        e236.setLayout(e236Layout);
        e236Layout.setHorizontalGroup(
            e236Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e236Layout.setVerticalGroup(
            e236Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e237.setBackground(new java.awt.Color(103, 91, 63));
        e237.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e237Layout = new javax.swing.GroupLayout(e237);
        e237.setLayout(e237Layout);
        e237Layout.setHorizontalGroup(
            e237Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e237Layout.setVerticalGroup(
            e237Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e238.setBackground(new java.awt.Color(103, 91, 63));
        e238.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e238Layout = new javax.swing.GroupLayout(e238);
        e238.setLayout(e238Layout);
        e238Layout.setHorizontalGroup(
            e238Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e238Layout.setVerticalGroup(
            e238Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e239.setBackground(new java.awt.Color(103, 91, 63));
        e239.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e239Layout = new javax.swing.GroupLayout(e239);
        e239.setLayout(e239Layout);
        e239Layout.setHorizontalGroup(
            e239Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e239Layout.setVerticalGroup(
            e239Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e240.setBackground(new java.awt.Color(103, 91, 63));
        e240.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e240Layout = new javax.swing.GroupLayout(e240);
        e240.setLayout(e240Layout);
        e240Layout.setHorizontalGroup(
            e240Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e240Layout.setVerticalGroup(
            e240Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e241.setBackground(new java.awt.Color(103, 91, 63));
        e241.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e241Layout = new javax.swing.GroupLayout(e241);
        e241.setLayout(e241Layout);
        e241Layout.setHorizontalGroup(
            e241Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e241Layout.setVerticalGroup(
            e241Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e242.setBackground(new java.awt.Color(103, 91, 63));
        e242.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e242Layout = new javax.swing.GroupLayout(e242);
        e242.setLayout(e242Layout);
        e242Layout.setHorizontalGroup(
            e242Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e242Layout.setVerticalGroup(
            e242Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e243.setBackground(new java.awt.Color(103, 91, 63));
        e243.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e243Layout = new javax.swing.GroupLayout(e243);
        e243.setLayout(e243Layout);
        e243Layout.setHorizontalGroup(
            e243Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e243Layout.setVerticalGroup(
            e243Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e244.setBackground(new java.awt.Color(103, 91, 63));
        e244.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e244Layout = new javax.swing.GroupLayout(e244);
        e244.setLayout(e244Layout);
        e244Layout.setHorizontalGroup(
            e244Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e244Layout.setVerticalGroup(
            e244Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e245.setBackground(new java.awt.Color(103, 91, 63));
        e245.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e245Layout = new javax.swing.GroupLayout(e245);
        e245.setLayout(e245Layout);
        e245Layout.setHorizontalGroup(
            e245Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e245Layout.setVerticalGroup(
            e245Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e246.setBackground(new java.awt.Color(103, 91, 63));
        e246.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e246Layout = new javax.swing.GroupLayout(e246);
        e246.setLayout(e246Layout);
        e246Layout.setHorizontalGroup(
            e246Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e246Layout.setVerticalGroup(
            e246Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e247.setBackground(new java.awt.Color(103, 91, 63));
        e247.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e247Layout = new javax.swing.GroupLayout(e247);
        e247.setLayout(e247Layout);
        e247Layout.setHorizontalGroup(
            e247Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e247Layout.setVerticalGroup(
            e247Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e248.setBackground(new java.awt.Color(103, 91, 63));
        e248.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e248Layout = new javax.swing.GroupLayout(e248);
        e248.setLayout(e248Layout);
        e248Layout.setHorizontalGroup(
            e248Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e248Layout.setVerticalGroup(
            e248Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e249.setBackground(new java.awt.Color(103, 91, 63));
        e249.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e249Layout = new javax.swing.GroupLayout(e249);
        e249.setLayout(e249Layout);
        e249Layout.setHorizontalGroup(
            e249Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e249Layout.setVerticalGroup(
            e249Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e250.setBackground(new java.awt.Color(103, 91, 63));
        e250.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e250Layout = new javax.swing.GroupLayout(e250);
        e250.setLayout(e250Layout);
        e250Layout.setHorizontalGroup(
            e250Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e250Layout.setVerticalGroup(
            e250Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e251.setBackground(new java.awt.Color(103, 91, 63));
        e251.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e251Layout = new javax.swing.GroupLayout(e251);
        e251.setLayout(e251Layout);
        e251Layout.setHorizontalGroup(
            e251Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e251Layout.setVerticalGroup(
            e251Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e252.setBackground(new java.awt.Color(103, 91, 63));
        e252.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e252Layout = new javax.swing.GroupLayout(e252);
        e252.setLayout(e252Layout);
        e252Layout.setHorizontalGroup(
            e252Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e252Layout.setVerticalGroup(
            e252Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e253.setBackground(new java.awt.Color(103, 91, 63));
        e253.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e253Layout = new javax.swing.GroupLayout(e253);
        e253.setLayout(e253Layout);
        e253Layout.setHorizontalGroup(
            e253Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e253Layout.setVerticalGroup(
            e253Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e254.setBackground(new java.awt.Color(103, 91, 63));
        e254.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e254Layout = new javax.swing.GroupLayout(e254);
        e254.setLayout(e254Layout);
        e254Layout.setHorizontalGroup(
            e254Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e254Layout.setVerticalGroup(
            e254Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e255.setBackground(new java.awt.Color(103, 91, 63));
        e255.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e255Layout = new javax.swing.GroupLayout(e255);
        e255.setLayout(e255Layout);
        e255Layout.setHorizontalGroup(
            e255Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e255Layout.setVerticalGroup(
            e255Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e256.setBackground(new java.awt.Color(103, 91, 63));
        e256.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e256Layout = new javax.swing.GroupLayout(e256);
        e256.setLayout(e256Layout);
        e256Layout.setHorizontalGroup(
            e256Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e256Layout.setVerticalGroup(
            e256Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e257.setBackground(new java.awt.Color(103, 91, 63));
        e257.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e257Layout = new javax.swing.GroupLayout(e257);
        e257.setLayout(e257Layout);
        e257Layout.setHorizontalGroup(
            e257Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e257Layout.setVerticalGroup(
            e257Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e258.setBackground(new java.awt.Color(103, 91, 63));
        e258.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e258Layout = new javax.swing.GroupLayout(e258);
        e258.setLayout(e258Layout);
        e258Layout.setHorizontalGroup(
            e258Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e258Layout.setVerticalGroup(
            e258Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e259.setBackground(new java.awt.Color(103, 91, 63));
        e259.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e259Layout = new javax.swing.GroupLayout(e259);
        e259.setLayout(e259Layout);
        e259Layout.setHorizontalGroup(
            e259Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e259Layout.setVerticalGroup(
            e259Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e260.setBackground(new java.awt.Color(103, 91, 63));
        e260.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e260Layout = new javax.swing.GroupLayout(e260);
        e260.setLayout(e260Layout);
        e260Layout.setHorizontalGroup(
            e260Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e260Layout.setVerticalGroup(
            e260Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e261.setBackground(new java.awt.Color(103, 91, 63));
        e261.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e261Layout = new javax.swing.GroupLayout(e261);
        e261.setLayout(e261Layout);
        e261Layout.setHorizontalGroup(
            e261Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e261Layout.setVerticalGroup(
            e261Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e262.setBackground(new java.awt.Color(103, 91, 63));
        e262.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e262Layout = new javax.swing.GroupLayout(e262);
        e262.setLayout(e262Layout);
        e262Layout.setHorizontalGroup(
            e262Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e262Layout.setVerticalGroup(
            e262Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e263.setBackground(new java.awt.Color(103, 91, 63));
        e263.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e263Layout = new javax.swing.GroupLayout(e263);
        e263.setLayout(e263Layout);
        e263Layout.setHorizontalGroup(
            e263Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e263Layout.setVerticalGroup(
            e263Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        e264.setBackground(new java.awt.Color(103, 91, 63));
        e264.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

        javax.swing.GroupLayout e264Layout = new javax.swing.GroupLayout(e264);
        e264.setLayout(e264Layout);
        e264Layout.setHorizontalGroup(
            e264Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );
        e264Layout.setVerticalGroup(
            e264Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout panSuperLayout = new javax.swing.GroupLayout(panSuper);
        panSuper.setLayout(panSuperLayout);
        panSuperLayout.setHorizontalGroup(
            panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panSuperLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(e264, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(e262, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(e261, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e258, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e256, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e260, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e259, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e263, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e255, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e257, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(e254, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(e252, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(e251, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e248, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e246, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e250, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e249, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e253, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e245, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e247, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(0, 0, 0)
                .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panSuperLayout.createSequentialGroup()
                        .addComponent(e195, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e196, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e202, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e201, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e198, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e197, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e200, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e199, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e203, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e204, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panSuperLayout.createSequentialGroup()
                        .addComponent(e182, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e183, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e187, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e188, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e189, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e190, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e191, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e192, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e193, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(e194, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panSuperLayout.createSequentialGroup()
                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e61, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e62, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e71, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e72, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e81, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e82, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e91, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e92, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e102, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e111, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e112, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e121, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e122, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e131, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e132, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e141, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e142, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e151, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e152, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e161, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e162, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e171, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e172, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e51, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e52, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(e3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(e23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e63, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e73, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e83, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e93, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e113, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e123, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e133, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e143, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e153, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e163, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e173, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(e24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e64, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e74, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e84, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e94, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e114, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e124, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e134, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e144, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e154, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e164, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e174, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e54, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e65, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e66, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e67, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e68, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e69, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e70, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e75, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e76, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e77, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e78, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e79, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e80, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e85, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e86, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e87, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e88, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e89, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e90, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e95, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e96, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e97, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e98, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e99, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e100, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e107, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e108, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e109, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e110, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e115, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e116, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e117, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e118, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e119, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e120, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e125, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e126, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e127, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e128, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e129, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e130, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e135, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e136, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e137, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e138, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e139, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e140, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e145, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e146, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e147, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e148, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e149, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e150, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e155, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e156, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e157, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e158, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e159, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e160, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e165, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e166, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e167, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e168, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e169, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e170, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e175, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e176, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e177, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e178, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e179, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e180, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addComponent(e55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e56, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e58, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e59, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(e60, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addGap(0, 0, 0)
                .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(e244, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(e242, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(e241, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e238, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e236, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e240, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e239, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e243, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e235, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e237, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(e234, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(e232, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(e231, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e228, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e226, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e230, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e229, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e233, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e225, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(e227, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(12, 12, 12)
                .addComponent(panLateral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panSuperLayout.setVerticalGroup(
            panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panSuperLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panLateral, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panSuperLayout.createSequentialGroup()
                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e247, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e245, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e253, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e249, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e250, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e246, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e248, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e251, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e254, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e252, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e257, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e255, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e263, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e259, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e260, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e256, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e258, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e261, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e264, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e262, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addComponent(e227, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e225, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e233, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e229, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e230, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e226, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e228, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e231, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e234, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e232, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e237, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e235, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e243, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e239, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e240, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e236, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e238, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e241, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e244, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(e242, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panSuperLayout.createSequentialGroup()
                                .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(e195, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e196, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e202, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e201, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e198, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e197, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e200, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e199, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e203, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e204, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panSuperLayout.createSequentialGroup()
                                        .addGap(16, 16, 16)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e60, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e59, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e58, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e56, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e52, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e51, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e70, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e69, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e68, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e67, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e66, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e65, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e62, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e61, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e80, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e79, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e78, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e77, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e76, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e75, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e72, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e71, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e90, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e89, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e88, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e87, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e86, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e85, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e82, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e81, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e100, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e99, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e98, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e97, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e96, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e95, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e92, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e91, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e110, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e109, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e108, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e107, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e102, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e120, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e119, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e118, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e117, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e116, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e115, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e112, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e111, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e130, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e129, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e128, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e127, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e126, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e125, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e122, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e121, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e140, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e139, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e138, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e137, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e136, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e135, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e132, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e131, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e150, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e149, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e148, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e147, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e146, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e145, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e142, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e141, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e160, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e159, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e158, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e157, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e156, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e155, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e152, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e151, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e170, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e169, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e168, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e167, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e166, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e165, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e162, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e161, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e180, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e179, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e178, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e177, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e176, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e175, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e172, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e171, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(panSuperLayout.createSequentialGroup()
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(e10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(e4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(panSuperLayout.createSequentialGroup()
                                                .addComponent(e14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e54, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e64, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e74, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e84, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e94, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e114, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e124, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e134, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e144, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e154, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e164, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e174, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panSuperLayout.createSequentialGroup()
                                                .addComponent(e13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e63, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e73, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e83, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e93, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e113, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e123, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e133, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e143, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e153, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e163, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(e173, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGap(0, 0, 0)
                                .addGroup(panSuperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(e182, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e183, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e187, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e188, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e189, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e190, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e191, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e192, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e193, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e194, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panSuper, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panSuper, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        if (evt.getKeyCode() == evt.VK_RIGHT) {
            moverDerecha();
        }
        if (evt.getKeyCode() == evt.VK_LEFT) {
            moverIzquierda();
        }
        if (evt.getKeyCode() == evt.VK_UP) {
            rotarPieza();
            colorear();
        }
        if (evt.getKeyCode() == evt.VK_DOWN) {
            bajar();
        }
    }//GEN-LAST:event_formKeyPressed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel e1;
    private javax.swing.JPanel e10;
    private javax.swing.JPanel e100;
    private javax.swing.JPanel e101;
    private javax.swing.JPanel e102;
    private javax.swing.JPanel e103;
    private javax.swing.JPanel e104;
    private javax.swing.JPanel e105;
    private javax.swing.JPanel e106;
    private javax.swing.JPanel e107;
    private javax.swing.JPanel e108;
    private javax.swing.JPanel e109;
    private javax.swing.JPanel e11;
    private javax.swing.JPanel e110;
    private javax.swing.JPanel e111;
    private javax.swing.JPanel e112;
    private javax.swing.JPanel e113;
    private javax.swing.JPanel e114;
    private javax.swing.JPanel e115;
    private javax.swing.JPanel e116;
    private javax.swing.JPanel e117;
    private javax.swing.JPanel e118;
    private javax.swing.JPanel e119;
    private javax.swing.JPanel e12;
    private javax.swing.JPanel e120;
    private javax.swing.JPanel e121;
    private javax.swing.JPanel e122;
    private javax.swing.JPanel e123;
    private javax.swing.JPanel e124;
    private javax.swing.JPanel e125;
    private javax.swing.JPanel e126;
    private javax.swing.JPanel e127;
    private javax.swing.JPanel e128;
    private javax.swing.JPanel e129;
    private javax.swing.JPanel e13;
    private javax.swing.JPanel e130;
    private javax.swing.JPanel e131;
    private javax.swing.JPanel e132;
    private javax.swing.JPanel e133;
    private javax.swing.JPanel e134;
    private javax.swing.JPanel e135;
    private javax.swing.JPanel e136;
    private javax.swing.JPanel e137;
    private javax.swing.JPanel e138;
    private javax.swing.JPanel e139;
    private javax.swing.JPanel e14;
    private javax.swing.JPanel e140;
    private javax.swing.JPanel e141;
    private javax.swing.JPanel e142;
    private javax.swing.JPanel e143;
    private javax.swing.JPanel e144;
    private javax.swing.JPanel e145;
    private javax.swing.JPanel e146;
    private javax.swing.JPanel e147;
    private javax.swing.JPanel e148;
    private javax.swing.JPanel e149;
    private javax.swing.JPanel e15;
    private javax.swing.JPanel e150;
    private javax.swing.JPanel e151;
    private javax.swing.JPanel e152;
    private javax.swing.JPanel e153;
    private javax.swing.JPanel e154;
    private javax.swing.JPanel e155;
    private javax.swing.JPanel e156;
    private javax.swing.JPanel e157;
    private javax.swing.JPanel e158;
    private javax.swing.JPanel e159;
    private javax.swing.JPanel e16;
    private javax.swing.JPanel e160;
    private javax.swing.JPanel e161;
    private javax.swing.JPanel e162;
    private javax.swing.JPanel e163;
    private javax.swing.JPanel e164;
    private javax.swing.JPanel e165;
    private javax.swing.JPanel e166;
    private javax.swing.JPanel e167;
    private javax.swing.JPanel e168;
    private javax.swing.JPanel e169;
    private javax.swing.JPanel e17;
    private javax.swing.JPanel e170;
    private javax.swing.JPanel e171;
    private javax.swing.JPanel e172;
    private javax.swing.JPanel e173;
    private javax.swing.JPanel e174;
    private javax.swing.JPanel e175;
    private javax.swing.JPanel e176;
    private javax.swing.JPanel e177;
    private javax.swing.JPanel e178;
    private javax.swing.JPanel e179;
    private javax.swing.JPanel e18;
    private javax.swing.JPanel e180;
    private javax.swing.JPanel e182;
    private javax.swing.JPanel e183;
    private javax.swing.JPanel e187;
    private javax.swing.JPanel e188;
    private javax.swing.JPanel e189;
    private javax.swing.JPanel e19;
    private javax.swing.JPanel e190;
    private javax.swing.JPanel e191;
    private javax.swing.JPanel e192;
    private javax.swing.JPanel e193;
    private javax.swing.JPanel e194;
    private javax.swing.JPanel e195;
    private javax.swing.JPanel e196;
    private javax.swing.JPanel e197;
    private javax.swing.JPanel e198;
    private javax.swing.JPanel e199;
    private javax.swing.JPanel e2;
    private javax.swing.JPanel e20;
    private javax.swing.JPanel e200;
    private javax.swing.JPanel e201;
    private javax.swing.JPanel e202;
    private javax.swing.JPanel e203;
    private javax.swing.JPanel e204;
    private javax.swing.JPanel e21;
    private javax.swing.JPanel e22;
    private javax.swing.JPanel e225;
    private javax.swing.JPanel e226;
    private javax.swing.JPanel e227;
    private javax.swing.JPanel e228;
    private javax.swing.JPanel e229;
    private javax.swing.JPanel e23;
    private javax.swing.JPanel e230;
    private javax.swing.JPanel e231;
    private javax.swing.JPanel e232;
    private javax.swing.JPanel e233;
    private javax.swing.JPanel e234;
    private javax.swing.JPanel e235;
    private javax.swing.JPanel e236;
    private javax.swing.JPanel e237;
    private javax.swing.JPanel e238;
    private javax.swing.JPanel e239;
    private javax.swing.JPanel e24;
    private javax.swing.JPanel e240;
    private javax.swing.JPanel e241;
    private javax.swing.JPanel e242;
    private javax.swing.JPanel e243;
    private javax.swing.JPanel e244;
    private javax.swing.JPanel e245;
    private javax.swing.JPanel e246;
    private javax.swing.JPanel e247;
    private javax.swing.JPanel e248;
    private javax.swing.JPanel e249;
    private javax.swing.JPanel e25;
    private javax.swing.JPanel e250;
    private javax.swing.JPanel e251;
    private javax.swing.JPanel e252;
    private javax.swing.JPanel e253;
    private javax.swing.JPanel e254;
    private javax.swing.JPanel e255;
    private javax.swing.JPanel e256;
    private javax.swing.JPanel e257;
    private javax.swing.JPanel e258;
    private javax.swing.JPanel e259;
    private javax.swing.JPanel e26;
    private javax.swing.JPanel e260;
    private javax.swing.JPanel e261;
    private javax.swing.JPanel e262;
    private javax.swing.JPanel e263;
    private javax.swing.JPanel e264;
    private javax.swing.JPanel e27;
    private javax.swing.JPanel e28;
    private javax.swing.JPanel e29;
    private javax.swing.JPanel e3;
    private javax.swing.JPanel e30;
    private javax.swing.JPanel e31;
    private javax.swing.JPanel e32;
    private javax.swing.JPanel e33;
    private javax.swing.JPanel e34;
    private javax.swing.JPanel e35;
    private javax.swing.JPanel e36;
    private javax.swing.JPanel e37;
    private javax.swing.JPanel e38;
    private javax.swing.JPanel e39;
    private javax.swing.JPanel e4;
    private javax.swing.JPanel e40;
    private javax.swing.JPanel e41;
    private javax.swing.JPanel e42;
    private javax.swing.JPanel e43;
    private javax.swing.JPanel e44;
    private javax.swing.JPanel e45;
    private javax.swing.JPanel e46;
    private javax.swing.JPanel e47;
    private javax.swing.JPanel e48;
    private javax.swing.JPanel e49;
    private javax.swing.JPanel e5;
    private javax.swing.JPanel e50;
    private javax.swing.JPanel e51;
    private javax.swing.JPanel e52;
    private javax.swing.JPanel e53;
    private javax.swing.JPanel e54;
    private javax.swing.JPanel e55;
    private javax.swing.JPanel e56;
    private javax.swing.JPanel e57;
    private javax.swing.JPanel e58;
    private javax.swing.JPanel e59;
    private javax.swing.JPanel e6;
    private javax.swing.JPanel e60;
    private javax.swing.JPanel e61;
    private javax.swing.JPanel e62;
    private javax.swing.JPanel e63;
    private javax.swing.JPanel e64;
    private javax.swing.JPanel e65;
    private javax.swing.JPanel e66;
    private javax.swing.JPanel e67;
    private javax.swing.JPanel e68;
    private javax.swing.JPanel e69;
    private javax.swing.JPanel e7;
    private javax.swing.JPanel e70;
    private javax.swing.JPanel e71;
    private javax.swing.JPanel e72;
    private javax.swing.JPanel e73;
    private javax.swing.JPanel e74;
    private javax.swing.JPanel e75;
    private javax.swing.JPanel e76;
    private javax.swing.JPanel e77;
    private javax.swing.JPanel e78;
    private javax.swing.JPanel e79;
    private javax.swing.JPanel e8;
    private javax.swing.JPanel e80;
    private javax.swing.JPanel e81;
    private javax.swing.JPanel e82;
    private javax.swing.JPanel e83;
    private javax.swing.JPanel e84;
    private javax.swing.JPanel e85;
    private javax.swing.JPanel e86;
    private javax.swing.JPanel e87;
    private javax.swing.JPanel e88;
    private javax.swing.JPanel e89;
    private javax.swing.JPanel e9;
    private javax.swing.JPanel e90;
    private javax.swing.JPanel e91;
    private javax.swing.JPanel e92;
    private javax.swing.JPanel e93;
    private javax.swing.JPanel e94;
    private javax.swing.JPanel e95;
    private javax.swing.JPanel e96;
    private javax.swing.JPanel e97;
    private javax.swing.JPanel e98;
    private javax.swing.JPanel e99;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel lat1;
    private javax.swing.JPanel lat10;
    private javax.swing.JPanel lat13;
    private javax.swing.JPanel lat14;
    private javax.swing.JPanel lat15;
    private javax.swing.JPanel lat16;
    private javax.swing.JPanel lat17;
    private javax.swing.JPanel lat18;
    private javax.swing.JPanel lat2;
    private javax.swing.JPanel lat21;
    private javax.swing.JPanel lat22;
    private javax.swing.JPanel lat23;
    private javax.swing.JPanel lat24;
    private javax.swing.JPanel lat5;
    private javax.swing.JPanel lat6;
    private javax.swing.JPanel lat7;
    private javax.swing.JPanel lat8;
    private javax.swing.JPanel lat9;
    private javax.swing.JPanel let11;
    private javax.swing.JPanel let12;
    private javax.swing.JPanel let19;
    private javax.swing.JPanel let20;
    private javax.swing.JPanel let3;
    private javax.swing.JPanel let4;
    private javax.swing.JPanel panLateral;
    private javax.swing.JPanel panSuper;
    private javax.swing.JTextField txtPuntos;
    // End of variables declaration//GEN-END:variables

    public String getNombre() {
        return nombre;
    }
}
